package com.srs.recommender

import org.apache.spark.SparkConf
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}

import java.sql.{Date, Timestamp}
import java.text.SimpleDateFormat
import java.util.Properties
import org.elasticsearch.action.admin.indices.create.CreateIndexRequest
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest
import org.elasticsearch.action.admin.indices.exists.indices.IndicesExistsRequest
import org.elasticsearch.common.settings.Settings
import org.elasticsearch.common.transport.InetSocketTransportAddress
import org.elasticsearch.transport.client.PreBuiltTransportClient

import java.net.InetAddress


case class Resource(r_id:Int, resource_name:String, resource_format:String,
                    type_id:Int, provider:String, description:String,
                    uuid:String, upload_time:Timestamp, add_time:Timestamp,
                    modified_time:Timestamp,size:Long)
case class Status(s_id:Int,r_id:Int,like:Int,downloads:Int,status:String)
case class Rating(user_id:Int,r_id:Int,score:Double,comment:String,time:Timestamp)
case class Tag(user_id:Int,r_id:Int,tag:String,time:Timestamp)

case class Type(type_id:Int,type_name:String,resource_total:Int,add_date:Date,
                modified_date:Date,total_like:Int,total_downloads:Int)

case class MysqlConfig(url:String,driver:String,user:String,password:String)
case class ESConfig(httpHosts:String, transportHosts:String, index:String,
                    clusterName:String)

object DataLoader {
  // 以 window 下为例，需替换成自己的路径，linux 下为 /YOUR_PATH/resources/movies.csv
  val RESOURCE_DATA_PATH = "D:\\Project\\Java\\LearnRecommendSystem\\recommender\\DataLoader\\src\\main\\resources\\repo.csv"
  val STATUS_DATA_PATH = "D:\\Project\\Java\\LearnRecommendSystem\\recommender\\DataLoader\\src\\main\\resources\\status.csv"
  val RATING_DATA_PATH = "D:\\Project\\Java\\LearnRecommendSystem\\recommender\\DataLoader\\src\\main\\resources\\ratings.csv"
  val TAG_DATA_PATH = "D:\\Project\\Java\\LearnRecommendSystem\\recommender\\DataLoader\\src\\main\\resources\\tags.csv"
  val TYPE_DATA_PATH = "D:\\Project\\Java\\LearnRecommendSystem\\recommender\\DataLoader\\src\\main\\resources\\type.csv"

  val MYSQL_RESOURCE_TABLE = "resource_repo"
  val MYSQL_STATUS_TABLE = "resource_status"
  val MYSQL_RATINGS_TABLE = "resource_ratings"
  val MYSQL_TAG_TABLE = "resource_tags"
  val MYSQL_TYPE_TABLE = "resource_type"
  val ES_RESOURCE_INDEX = "Resource"

  def main(args: Array[String]): Unit = {

    val config = Map(
      "spark.cores" -> "local[*]",
      "mysql.driver" -> "com.mysql.cj.jdbc.Driver",
      "mysql.url" -> "jdbc:mysql://192.168.10.110:3306/learn_recommender?useSSL=false&characterEncoding=utf8&useUnicode=true&serverTimezone=Asia/Shanghai",
      "mysql.user" -> "root",
      "mysql.password" -> "Gzl5411258.",
      "es.httpHosts" -> "192.168.10.110:9200",
      "es.transportHosts" -> "192.168.10.110:9300",
      "es.index" -> "recommender",
      "es.cluster.name" -> "es-cluster"
    )

    // 创建SparkConf
    val sparkConf = new SparkConf().setAppName("DataLoader").setMaster(config("spark.cores"))

    // 创建SparkSession
    val spark = SparkSession.builder().config(sparkConf).getOrCreate()

    import spark.implicits._

    // 加载数据
    val resourceRDD = spark.sparkContext.textFile(RESOURCE_DATA_PATH)
    val typeRDD = spark.sparkContext.textFile(TYPE_DATA_PATH)
    val statusRDD = spark.sparkContext.textFile(STATUS_DATA_PATH)
    val tagsRDD = spark.sparkContext.textFile(TAG_DATA_PATH)
    val ratingsRDD = spark.sparkContext.textFile(RATING_DATA_PATH)

    val format = new SimpleDateFormat("yyyy/MM/dd HH:mm")
    val resourceDF = resourceRDD.map(
      item => {
        val attr = item.split(",")
        val ut = new Timestamp(format.parse(attr(7).trim).getTime)
        val at = new Timestamp(format.parse(attr(8).trim).getTime)
        val mt = new Timestamp(format.parse(attr(9).trim).getTime)
        Resource(attr(0).trim.toInt,attr(1).trim,attr(2).trim,attr(3).trim.toInt,
          attr(4).trim,attr(5).trim,attr(6).trim,ut,at,
          mt,attr(10).trim.trim.toLong)
      }
    ).toDF()

    val typeDF = typeRDD.map(
      item => {
        val attr = item.split(",")
        val at = new Date(format.parse(attr(3).trim).getTime)
        val mt = new Date(format.parse(attr(4).trim).getTime)
        Type(attr(0).toInt,attr(1).trim,attr(2).toInt,at,mt,
          attr(5).toInt,attr(6).toInt)
      }
    ).toDF()

    val statusDF = statusRDD.map(
      item => {
        val attr = item.split(",")
        Status(attr(0).toInt,attr(1).toInt,attr(2).toInt,attr(3).toInt,attr(4).trim)
      }
    ).toDF()

    val tagsDF = tagsRDD.map(
      item => {
        val attr = item.split(",")
        Tag(attr(0).toInt,attr(1).toInt,attr(2).trim,new Timestamp(System.currentTimeMillis()))
      }
    ).toDF()

    val ratingsDF = ratingsRDD.map(
      item => {
        val attr = item.split(",")
        Rating(attr(0).toInt,attr(1).toInt,attr(2).toInt,"测试评价",new Timestamp(System.currentTimeMillis()))
      }
    ).toDF()

//    resourceDF.show(5)
//    typeDF.show(5)
//    statusDF.show(10)
//    tagsDF.show(10)
//    ratingsDF.show(10)

    val mysqlConfig: MysqlConfig = MysqlConfig(config("mysql.url"),config("mysql.driver"),config("mysql.user"),config("mysql.password"))

    // 数据保存Mysql
//    storeDataInMysqlDB(resourceDF,MYSQL_RESOURCE_TABLE,mysqlConfig)
//    storeDataInMysqlDB(typeDF,MYSQL_TYPE_TABLE,mysqlConfig)
//    storeDataInMysqlDB(statusDF,MYSQL_STATUS_TABLE,mysqlConfig)
//    storeDataInMysqlDB(tagsDF,MYSQL_TAG_TABLE,mysqlConfig)
//    storeDataInMysqlDB(ratingsDF,MYSQL_RATINGS_TABLE,mysqlConfig)


    // 数据预处理
    import org.apache.spark.sql.functions._
    val newTag = tagsDF.groupBy($"r_id")
      .agg(concat_ws("|",collect_set($"tag"))
        .as("tags"))
      .select("r_id","tags")
//      newTag.show()

    val resourceWithTagsDF = resourceDF.join(newTag,Seq("r_id"),"left")
//    resourceWithTagsDF.show()

    // 声明了一个 ES 配置的隐式参数
    val esConfig = ESConfig(config("es.httpHosts"),
      config("es.transportHosts"),
      config("es.index"),
      config("es.cluster.name"))


    // 保存数据到ES
    storeDataInES(resourceWithTagsDF,esConfig)
    // 关闭连接
    spark.stop()
  }

  def storeDataInMysqlDB(df: DataFrame,tableName:String,mysqlConfig: MysqlConfig): Unit = {
    val mp = new Properties()
    mp.setProperty("user",mysqlConfig.user)
    mp.setProperty("password",mysqlConfig.password)
    df.write.mode(SaveMode.Append).jdbc(mysqlConfig.url,tableName,mp)
  }


  def storeDataInES(df:DataFrame, esConfig: ESConfig): Unit = {
    //新建一个配置
    val settings:Settings = Settings.builder()
      .put("cluster.name",esConfig.clusterName).build()
    //新建一个 ES 的客户端
    val esClient = new PreBuiltTransportClient(settings)

    //需要将 TransportHosts 添加到 esClient 中
    val REGEX_HOST_PORT = "(.+):(\\d+)".r
    esConfig.transportHosts.split(",").foreach{
      case REGEX_HOST_PORT(host:String,port:String) => {
        esClient.addTransportAddress(new
            InetSocketTransportAddress(InetAddress.getByName(host),port.toInt))
      }
    }
    //需要清除掉 ES 中遗留的数据
    if(esClient.admin().indices().exists(new
        IndicesExistsRequest(esConfig.index)).actionGet().isExists){
      esClient.admin().indices().delete(new DeleteIndexRequest(esConfig.index))
    }
    esClient.admin().indices().create(new CreateIndexRequest(esConfig.index))

    df.write
      .option("es.nodes",esConfig.httpHosts)
      .option("es.http.timeout","100m")
      .option("es.mapping.id","r_id")
      .mode("overwrite")
      .format("org.elasticsearch.spark.sql")
      .save(esConfig.index+"/"+ES_RESOURCE_INDEX)
  }
}
