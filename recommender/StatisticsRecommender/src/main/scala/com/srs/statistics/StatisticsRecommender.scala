package com.srs.statistics

import org.apache.spark.SparkConf
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}

import java.sql.{Date, Timestamp}
import java.text.SimpleDateFormat
import java.util.Properties

case class Resource(r_id:Int, resource_name:String, resource_format:String,
                    type_id:Int, provider:String, description:String,
                    uuid:String, upload_time:Timestamp, add_time:Timestamp,
                    modified_time:Timestamp,size:Long)
case class Status(s_id:Int,r_id:Int,like:Int,downloads:Int,status:String)
case class Rating(user_id:Int,r_id:Int,score:Double,comment:String,time:Timestamp)
case class Tag(user_id:Int,r_id:Int,tag:String,time:Timestamp)

case class MysqlConfig(url:String,driver:String,user:String,password:String)


object StatisticsRecommender {

  val MYSQL_RESOURCE_TABLE = "resource_repo"
  val MYSQL_RATING_TABLE = "resource_ratings"

  //统计的表的名称
  val RATE_MORE_RESOURCES = "rate_more_resources"
  val RATE_MORE_RECENTLY_RESOURCES = "rate_more_recently_resources"
  val AVERAGE_RESOURCE = "average_resources"

  def main(args: Array[String]): Unit = {

    val config = Map(
      "spark.cores" -> "local[*]",
      "mysql.driver" -> "com.mysql.cj.jdbc.Driver",
      "mysql.url" -> "jdbc:mysql://192.168.10.110:3306/learn_recommender?useSSL=false&characterEncoding=utf8&useUnicode=true&serverTimezone=Asia/Shanghai",
      "mysql.user" -> "root",
      "mysql.password" -> "Gzl5411258."
    )

    // 创建SparkConf
    val sparkConf = new SparkConf().setAppName("StatisticsRecommender").setMaster(config("spark.cores"))
    // 创建SparkSession
    val spark = SparkSession.builder().config(sparkConf).getOrCreate()

    val mysqlConfig = MysqlConfig(config("mysql.url"),config("mysql.driver"),config("mysql.user"),config("mysql.password"))

    // 加入隐式转换
    import spark.implicits._

    val ratingsDF = spark.read.format("jdbc")
      .option("url",config("mysql.url"))
      .option("driver",config("mysql.driver"))
      .option("user",config("mysql.user"))
      .option("password",config("mysql.password"))
      .option("dbtable",MYSQL_RATING_TABLE)
      .load()
      .as[Rating]
      .toDF()

    ratingsDF.createTempView("ratings")

    // TODO: 不同的统计推荐结果
    // 1.历史热门统计，历史评分数据最多
    val rateMoreResourceDF = spark.sql("select r_id,count(r_id) as count from ratings group by r_id")
//    rateMoreResourceDF.show()
//    storeDataInMysqlDB(rateMoreResourceDF,RATE_MORE_RESOURCES,mysqlConfig)

    // 2.近期热门数据，按照"yyyyMM"格式选取最近的评分数据，统计评分个数
    // 创建日期格式化工具
    val format = new SimpleDateFormat("yyyyMM")
    // 注册一个 UDF 函数，用于将 timestamp 装换成年月格式 1260759144000 => 201605
    spark.udf.register("changeDate",(x:Timestamp) => format.format(new Date(x.getTime)).toInt)
    // 对原始数据做预处理，去掉userId
    val ratingsOfYearMonth = spark.sql("select r_id, score, changeDate(time) as yearmonth from ratings")
    ratingsOfYearMonth.createTempView("ratingOfMonth")
    // 从ratingOfMonth中查找资源在各个月份的评分， rId,count,yearmonth
    val rateMoreRecentlyResourceDF = spark.sql("select r_id, count(r_id) as count, yearmonth from ratingOfMonth group by yearmonth, r_id order by yearmonth desc, count desc")
//    rateMoreRecentlyResourceDF.show()
//    storeDataInMysqlDB(rateMoreRecentlyResourceDF.withColumnRenamed("yearmonth","date"),RATE_MORE_RECENTLY_RESOURCES,mysqlConfig)

    // 3.优质资源统计，统计资源的平均得分,r_id,avg
    val averageResourceDF = spark.sql("select r_id,avg(score) as avg from ratings group by r_id order by avg desc")
//    averageResourceDF.show(20)
    storeDataInMysqlDB(averageResourceDF.withColumnRenamed("avg","avg_score"),AVERAGE_RESOURCE,mysqlConfig)
    // 关闭连接
    spark.stop()
  }

  def storeDataInMysqlDB(df: DataFrame,tableName:String,mysqlConfig: MysqlConfig): Unit = {
    val mp = new Properties()
    mp.setProperty("user",mysqlConfig.user)
    mp.setProperty("password",mysqlConfig.password)
    df.write.mode(SaveMode.Overwrite).jdbc(mysqlConfig.url,tableName,mp)
  }

}
