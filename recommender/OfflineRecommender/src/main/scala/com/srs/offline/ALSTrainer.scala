package com.srs.offline

import breeze.numerics.sqrt
import com.srs.offline.OfflineRecommender.MYSQL_RATING_TABLE
import org.apache.spark.SparkConf
import org.apache.spark.mllib.recommendation.{ALS, MatrixFactorizationModel, Rating}
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession


object ALSTrainer {

  def main(args: Array[String]): Unit = {

    val config = Map(
      "spark.cores" -> "local[*]",
      "mysql.driver" -> "com.mysql.cj.jdbc.Driver",
      "mysql.url" -> "jdbc:mysql://192.168.10.110:3306/learn_recommender?useSSL=false&characterEncoding=utf8&useUnicode=true&serverTimezone=Asia/Shanghai",
      "mysql.user" -> "root",
      "mysql.password" -> "Gzl5411258."
    )

    // 创建SparkConf
    val sparkConf: SparkConf = new SparkConf().setAppName("StatisticsRecommender").setMaster(config("spark.cores"))
    // 创建SparkSession
    val spark: SparkSession = SparkSession.builder().config(sparkConf).getOrCreate()

    val mysqlConfig: MysqlConfig = MysqlConfig(config("mysql.url"),config("mysql.driver"),config("mysql.user"),config("mysql.password"))

    // 加入隐式转换
    import spark.implicits._

    // 读取Mysql Rating数据
    val ratingsRDD: RDD[Rating] = spark
      .read.format("jdbc")
      .option("url",config("mysql.url"))
      .option("driver",config("mysql.driver"))
      .option("user",config("mysql.user"))
      .option("password",config("mysql.password"))
      .option("dbtable",MYSQL_RATING_TABLE)
      .load()
      .as[ResourceRating]
      // 转换成rdd 并去掉time和comment
      .rdd
      .map(rating => Rating(rating.user_id, rating.r_id, rating.score) )
      .cache()

    // 划分数据集
    val sp = ratingsRDD.randomSplit(Array(0.8,0.2))
    val trainingRDD = sp(0)
    val testingRDD = sp(1)

    // 输出最优参数
    adjustALSParam(trainingRDD,testingRDD)

    // 关闭spark
    spark.close()
  }

  // 输出最终的最优参数
  def adjustALSParam(trainData:RDD[Rating], testData:RDD[Rating]):Unit ={
    // 这里指定迭代次数为 10，rank 和 lambda 在几个值中选取调整
    val result = for(rank <- Array(10,20,50,100,150,200,250); lambda <- Array(1,0.1,0.01))
      yield {
        val model =ALS.train(trainData,rank,10,lambda)
        val rmse = getRMSE(model,testData)
        (rank,lambda,rmse)
      }
    // 按照rmse排序
    val sortRe =  result.sortBy(_._3)
    println("最佳参数：" + sortRe.head)
    println("-----------------------")
    println("所有结果：")
    sortRe.foreach(println)
  }

  // 计算均方差
  def getRMSE(model: MatrixFactorizationModel, data: RDD[Rating]): Double = {
    val userResource = data.map(item => (item.user, item.product))
    val predictRating = model.predict(userResource)
    val realL: RDD[((Int, Int), Double)] = data.map(item => ((item.user,item.product),item.rating))
    val predictL = predictRating.map(item => ((item.user,item.product),item.rating))
    // 计算 RMSE
    sqrt(
      realL.join(predictL).map{
        case ((uid,rid),(actual,pre)) =>
          // 真实值和预测值之间的差
          val err = actual - pre
          err * err
      }.mean()
    )
  }

}
