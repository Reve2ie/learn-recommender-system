package com.srs.offline

import org.apache.spark.SparkConf
import org.apache.spark.mllib.recommendation.{ALS, Rating}
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}
import org.jblas.DoubleMatrix

import java.sql.Timestamp
import java.util.Properties

case class Resource(r_id:Int, resource_name:String, resource_format:String,
                    type_id:Int, provider:String, description:String,
                    uuid:String, upload_time:Timestamp, add_time:Timestamp,
                    modified_time:Timestamp,size:Long)
case class ResourceRating(user_id:Int,r_id:Int,score:Double,comment:String,time:Timestamp)

case class MysqlConfig(url:String,driver:String,user:String,password:String)

// 标准推荐对象，rid,score
case class Recommendation(r_id: Int, score:Double)
// 用户推荐
case class UserRecs(user_id: Int, recs: Seq[Recommendation])
// 资源相似度（资源推荐）
case class ResourceRecs(r_id: Int, recs: Seq[Recommendation])

object OfflineRecommender {

  // 所需要数据的表
  val MYSQL_RESOURCE_TABLE = "resource_repo"
  val MYSQL_RATING_TABLE = "resource_ratings"
  // 推荐表的名称
  val USER_RECS = "user_recs"
  val Resource_RECS = "resource_recs"

  val USER_MAX_RECOMMENDATION = 20

  def main(args: Array[String]): Unit = {

    val config = Map(
      "spark.cores" -> "local[*]",
      "mysql.driver" -> "com.mysql.cj.jdbc.Driver",
      "mysql.url" -> "jdbc:mysql://192.168.10.110:3306/learn_recommender?useSSL=false&characterEncoding=utf8&useUnicode=true&serverTimezone=Asia/Shanghai",
      "mysql.user" -> "root",
      "mysql.password" -> "Gzl5411258."
    )

    // 创建SparkConf
    val sparkConf = new SparkConf().setAppName("StatisticsRecommender").setMaster(config("spark.cores"))
    // 创建SparkSession
    val spark = SparkSession.builder().config(sparkConf).getOrCreate()

    val mysqlConfig = MysqlConfig(config("mysql.url"),config("mysql.driver"),config("mysql.user"),config("mysql.password"))

    // 加入隐式转换
    import spark.implicits._

    // 读取Mysql Rating数据
    val ratingsRDD = spark.read.format("jdbc")
      .option("url",config("mysql.url"))
      .option("driver",config("mysql.driver"))
      .option("user",config("mysql.user"))
      .option("password",config("mysql.password"))
      .option("dbtable",MYSQL_RATING_TABLE)
      .load()
      .as[ResourceRating]
      // 转换成rdd 并去掉time和comment
      .rdd
      .map(rating => (rating.user_id, rating.r_id, rating.score) )
      .cache()

    // 用户的数据rdd
    val userRDD = ratingsRDD.map(_._1).distinct()

    // 资源的数据rdd
//    val resourceRDD = spark
//      .read
//      .format("jdbc")
//      .option("url",config("mysql.url"))
//      .option("driver",config("mysql.driver"))
//      .option("user",config("mysql.user"))
//      .option("password",config("mysql.password"))
//      .option("dbtable",MYSQL_RESOURCE_TABLE)
//      .load()
//      .as[Resource]
//      .rdd
//      .map(_.r_id).cache()

    val resourceRDD = ratingsRDD.map(_._2).distinct()

//    ratingsRDD.take(5).foreach(println)
//    resourceRDD.take(5).foreach(println)

    /** 训练隐语义模型 */
    // 训练集数据
    val trainData = ratingsRDD.map(x => Rating(x._1,x._2,x._3))
    // 参数 rank 是模型中隐语义因子的个数, iterations 是迭代的次数, lambda 是 ALS 的正则化参数
    val (rank,iterations,lambda) = (200,10,1)
    // 训练隐语义模型
    val model = ALS.train(trainData,rank, iterations, lambda)

    // TODO 计算用户推荐矩阵
    // 用户与资源的笛卡尔集，空评分矩阵
    val userResource = userRDD.cartesian(resourceRDD)
    // 调用model的predict方法预测评分
    val preRatings = model.predict(userResource)

//    val userRecs = preRatings
//      .filter(_.rating > 0) // 过滤评分小于0的项
//      .map(rating => ( rating.user, (rating.product, rating.rating)))
//      .groupByKey()
//      .map{
//        case (user_id,recs) => UserRecs(user_id,recs.toList.sortWith(_._2 > _._2)
//        .take(USER_MAX_RECOMMENDATION).map(x => Recommendation(x._1,x._2)))
//      }
//      .toDF()


    val userRecsToDB = preRatings
      .filter(_.rating > 0) // 过滤评分小于0的项
      .map(rating => ( rating.user, (rating.product, rating.rating)))
      .groupByKey()
      .map{
        case (user_id,recs) => (user_id,recs.toList.sortWith(_._2 > _._2)
          .take(USER_MAX_RECOMMENDATION).map(x => (x._1,x._2)))
      }
      .flatMapValues(x => x)
      .map{
        case (a , (b , c )) => (a, b, c)
      }
      .toDF()
      .withColumnRenamed("_1","user_id")
      .withColumnRenamed("_2","r_id")
      .withColumnRenamed("_3","score")

//    storeDataInMysqlDB(userRecsToDB,USER_RECS,mysqlConfig)

//    userRecs.show()

    // TODO 计算用户推荐矩阵
    // 获取资源的特征矩阵
    val resourceFeatures = model.productFeatures.map{
      case (r_id,feature) => (r_id, new DoubleMatrix(feature))
    }

    // 计算笛卡儿积并过滤合并
    val resourceRecs = resourceFeatures.cartesian(resourceFeatures)
      .filter{ case (a,b) => a._1 != b._1}
      .map{ case (a,b) =>
        val simScore = this.consinSim(a._2,b._2) // 求余弦相似度
        (a._1,(b._1, simScore))
      }
      .filter(_._2._2 > 0.6)
      .groupByKey()
      .map{
        case (r_id,items) =>
          ResourceRecs(r_id,items.toList.map(x => Recommendation(x._1,x._2)))
      }.toDF()

//    val resourceRecsToDB = resourceFeatures.cartesian(resourceFeatures)
//      .filter{ case (a,b) => a._1 != b._1}
//      .map{ case (a,b) =>
//        val simScore = this.consinSim(a._2,b._2) // 求余弦相似度
//        (a._1,b._1, simScore)
//      }
//      .filter(_._3 > 0.6)
//      .toDF()
//      .withColumnRenamed("_1","r_id1")
//      .withColumnRenamed("_2","r_id2")
//      .withColumnRenamed("_3","score")

//    storeDataInMysqlDB(resourceRecsToDB,Resource_RECS,mysqlConfig)

//    resourceRecs.show(10)
//    resourceRecsToDB.show(10)

    spark.close()
  }

  // 计算两个资源之间的余弦相似度
  def consinSim(resource1:DoubleMatrix, resource2:DoubleMatrix): Double ={
    resource1.dot(resource2) / ( resource1.norm2() * resource2.norm2() )
  }

  def storeDataInMysqlDB(df: DataFrame,tableName:String,mysqlConfig: MysqlConfig): Unit = {
    val mp = new Properties()
    mp.setProperty("user",mysqlConfig.user)
    mp.setProperty("password",mysqlConfig.password)
    df.write.mode(SaveMode.Append).jdbc(mysqlConfig.url,tableName,mp)
  }

}
