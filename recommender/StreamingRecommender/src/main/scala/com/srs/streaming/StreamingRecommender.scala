package com.srs.streaming

import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.spark.SparkConf
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}
import org.apache.spark.streaming.kafka010.{ConsumerStrategies, KafkaUtils, LocationStrategies}
import org.apache.spark.streaming.{Seconds, StreamingContext}
import redis.clients.jedis.Jedis

import java.util.Properties
import scala.collection.JavaConversions._

// 连接助手对象
object ConnHelper extends Serializable{
  lazy val jedis = new Jedis("localhost")
  lazy val mysqlConfig: MysqlConfig = MysqlConfig("com.mysql.cj.jdbc.Driver"
    ,"jdbc:mysql://192.168.10.110:3306/learn_recommender?useSSL=false&characterEncoding=utf8&useUnicode=true&serverTimezone=Asia/Shanghai"
    ,"root"
    ,"Gzl5411258.")
}

case class MysqlConfig(url:String,driver:String,user:String,password:String) extends Serializable

// 标准推荐对象，rid,score
case class Recommendation(r_id: Int, score:Double)
// 用户推荐
case class UserRecs(user_id: Int, recs: Seq[Recommendation])
// 资源相似度（资源推荐）
case class ResourceRecs(r_id: Int, recs: Seq[Recommendation])

object StreamingRecommender {

  // 推荐表的名称
  val MYSQL_USER_RECS = "user_recs"
  val MYSQL_RESOURCE_RECS = "resource_recs"
  val MYSQL_STREAM_RECS_TABLE = "stream_recs"
  val MYSQL_RATING_TABLE = "resource_ratings"

  // 选取最大数量
  val MAX_USER_RATINGS_NUM = 20
  val MAX_SIM_RESOURCE_NUM = 20
  val USER_MAX_RECOMMENDATION = 20


  val config = Map(
    "spark.cores" -> "local[*]",
    "mysql.driver" -> "com.mysql.cj.jdbc.Driver",
    "mysql.url" -> "jdbc:mysql://192.168.10.110:3306/learn_recommender?useSSL=false&characterEncoding=utf8&useUnicode=true&serverTimezone=Asia/Shanghai",
    "mysql.user" -> "root",
    "mysql.password" -> "Gzl5411258.",
    "kafka.topic" -> "recommender"
  )


  def main(args: Array[String]): Unit = {

    // 创建SparkConf
    val sparkConf = new SparkConf().setAppName("StreamingRecommender").setMaster(config("spark.cores"))
    // SparkSession
    val ss = SparkSession.builder().config(sparkConf).getOrCreate()
    // SparkContext
//    val sc = ss.sparkContext
    // SparkStreaming
    val ssc = new StreamingContext(ss.sparkContext,Seconds(2))

    // mysql config


    // 加入隐式转换
    import ss.implicits._

    // 广播资源相似度矩阵
    // 转换成Map[Int,Map[Int,Double]
    val simResourceMatrix = ss
      .read.format("jdbc")
      .option("url",config("mysql.url"))
      .option("driver",config("mysql.driver"))
      .option("user",config("mysql.user"))
      .option("password",config("mysql.password"))
      .option("dbtable",MYSQL_RESOURCE_RECS)
      .load()
      .rdd
      .map(x=> (x.get(0).toString.toInt, (x.get(1).toString.toInt, x.get(2).toString.toDouble)))
      .groupByKey()
      .map{
        case (r_id,items) =>
          ResourceRecs(r_id,items.toList.map(x => Recommendation(x._1,x._2)))
      }
      .map{
        recs => (recs.r_id,recs.recs.map(x => (x.r_id,x.score)).toMap)
      }.collectAsMap()

//    simResourceMatrix.take(5).foreach(println)

    val simResourceMatrixBroadCast = ss.sparkContext.broadcast(simResourceMatrix)

    // 创建到kafka的连接
    val kafkaConfig = Map(
      "bootstrap.servers" -> "192.168.10.110:9092",
      "key.deserializer" -> classOf[StringDeserializer],
      "value.deserializer" -> classOf[StringDeserializer],
      "group.id" -> "recommender",
      "auto.offset.reset" -> "latest"
    )

    val kafkaStream = KafkaUtils.createDirectStream[String,String](ssc,LocationStrategies.PreferConsistent
    ,ConsumerStrategies.Subscribe[String,String](Array(config("kafka.topic")),kafkaConfig))

    // UID|RID|SCORE|TIMESTAMP
    // 产生评分流
    val ratingStream = kafkaStream.map {
      msg =>
        val attr = msg.value().split("\\|")
        (attr(0).toInt, attr(1).toInt, attr(2).toDouble, attr(3).toInt)
    }

    // 核心实时推荐算法
    ratingStream.foreachRDD{rdd =>
      rdd.map{
        case (uid,rid,score,timestamp) =>
          println(">>>>>>>>>>>>>>>>>>>>")

          // 获取当前最近的M次资源评分
          val userRecentlyRatings = getUserRecentlyRating(MAX_USER_RATINGS_NUM,uid,ConnHelper.jedis)

          // 获取资源R最相似的K个资源
//          val simResources = getTopSimResources(MAX_SIM_RESOURCE_NUM,rid,uid,ss,simResourceMatrixBroadCast.value)

          //从广播变量的资源相似度矩阵中获取当前资源所有的相似资源
          val allSimResource = simResourceMatrixBroadCast.value(rid).toArray

          // 获取用户已经评价过的资源
          val newss = SparkSession.builder().getOrCreate()
          val userRatedResource = newss
            .read.format("jdbc")
            .option("url",config("mysql.url"))
            .option("driver",config("mysql.driver"))
            .option("user",config("mysql.user"))
            .option("password",config("mysql.password"))
            .option("dbtable","(select * from resource_ratings where user_id=" + uid.toString + ") temp")
            .load()
            .rdd
            .map{ item => item.get(1).toString.toInt }
            .collect()

          // 过滤掉已经评分过得资源，并排序输出
//          val simResources = allSimResource.filter(x => !userRatedResource.contains(x._1)).sortWith(_._2 > _._2)
//            .take(MAX_SIM_RESOURCE_NUM).map(x => x._1)

          // 目前数据量小 只过滤用户当前评分资源ID
          val simResources = allSimResource.filter(_ => !userRatedResource.contains(rid)).sortWith(_._2 > _._2)
            .take(MAX_SIM_RESOURCE_NUM).map(x => x._1)

          // 计算待选电影的推荐优先级
          val streamRecs = computeResourceScores(simResourceMatrixBroadCast.value,userRecentlyRatings,simResources)

          println(streamRecs.mkString("RECS(", ", ", ")"))
          // redis 存放实时推荐数据
          ConnHelper.jedis.del("recs-uid:" + uid.toString)
          for (elem <- streamRecs) {
            ConnHelper.jedis.rpush("recs-uid:" + uid.toString,elem._1.toString+":"+elem._2.toString)
          }

// 目前无BUG解决方法 sparkContext 序列化问题
//          val streamRecsDF = newss.sparkContext.parallelize(streamRecs)
//            .map(x => (uid, x._1, x._2))
//            .toDF()
//            .withColumnRenamed("_1","user_id")
//            .withColumnRenamed("_2","r_id")
//            .withColumnRenamed("_3","score")
          // 将数据保存到Mysql
//          saveStreamRecsToMysqlDB(streamRecsDF,MYSQL_STREAM_RECS_TABLE,ConnHelper.mysqlConfig)
      }.count()
    }

    // 启动Streaming程序
    ssc.start()
    ssc.awaitTermination()
  }

  /**
   * 获取当前最近的K次资源评分
   * @param num 评分个数
   * @param uid 评分用户ID
   * @param jedis redis
   * @return
   */
  def getUserRecentlyRating(num:Int, uid:Int,jedis:Jedis):Array[(Int,Double)] ={
    // 从用户队列中取出num个评分
    jedis.lrange("uid:"+ uid.toString,0,num-1).map{
      item =>
        val attr = item.split("\\:")
        (attr(0).trim.toInt, attr(1).trim.toDouble)
    }
  }.toArray

  /**
   * 获取当前资源 K 个相似的资源
   * SparkContext 序列化问题BUG
   * @param num 相似资源数量
   * @param rid 资源ID
   * @param uid 用户ID
   * @param ss sparkSession
   * @param simResources 资源相似度矩阵的广播变量值
   * @return
   */
//  def getTopSimResources(num:Int, rid:Int, uid:Int, ss:SparkSession
//                         , simResources:scala.collection.Map[Int,scala.collection.immutable.Map[Int,Double]]):Array[Int] = {
//    //从广播变量的资源相似度矩阵中获取当前资源所有的相似资源
//    val allSimResource = simResources(rid).toArray
//    // 获取用户已经评价过的资源
//    val userRatedResource = ss
//      .read.format("jdbc")
//      .option("url",config("mysql.url"))
//      .option("driver",config("mysql.driver"))
//      .option("user",config("mysql.user"))
//      .option("password",config("mysql.password"))
//      .option("dbtable","(select * from resource_ratings where user_id=" + uid.toString + ") temp")
//      .load()
//      .rdd
//      .map{ item => item.get(1).toString.toInt }
//      .collect()
//    // 过滤掉已经评分过得资源，并排序输出
//    allSimResource.filter(x => !userRatedResource.contains(x._1)).sortWith(_._2 > _._2)
//      .take(num).map(x => x._1)
//  }

  /**
   * 计算待选资源的推荐分数
   * @param simResources  资源相似度矩阵  Map( RID , ( RID, SCORE ) )
   * @param userRecentlyRatings  用户最近的K次评分 ARRAY( RID , SCORE )
   * @param topSimResources  当前资源最相似的K个资源 RID
   * @return
   */
  def computeResourceScores(
                          simResources:scala.collection.Map[Int,scala.collection.immutable.Map[Int,Double]],
                          userRecentlyRatings:Array[(Int,Double)],
                          topSimResources: Array[Int]): Array[(Int,Double)] ={
    // 用于保存每一个待选资源和最近评分的每一个资源的权重得分
    val score = scala.collection.mutable.ArrayBuffer[(Int,Double)]()

    // 用于保存每一个资源的增强因子数
    val increMap = scala.collection.mutable.HashMap[Int,Int]()

    // 用于保存每一个资源的减弱因子数
    val decreMap = scala.collection.mutable.HashMap[Int,Int]()

    for(topSimResource <- topSimResources; userRecentlyRating <- userRecentlyRatings){
      val simScore = getResourceSimScore(simResources, userRecentlyRating._1, topSimResource)
      if(simScore > 0.7){
        score += ((topSimResource, simScore * userRecentlyRating._2))
        if(userRecentlyRating._2 > 5){
          increMap(topSimResource) = increMap.getOrDefault(topSimResource,0) + 1
        }else{
          decreMap(topSimResource) = decreMap.getOrDefault(topSimResource,0) + 1
        }
      }
    }

    score.groupBy(_._1).map{
      case (rid,sims) =>
        (rid,sims.map(_._2).sum / sims.length +  log(increMap.getOrDefault(rid, 1)) -
          log(decreMap.getOrDefault(rid, 1)))
    }.toArray.sortWith(_._2 > _._2)
  }

  /**
   * 获取当前资源之间的相似都
   * @param simResources 资源相似度矩阵
   * @param userRatingResource 用户已经评分的资源 RID
   * @param topSimResource 获选资源 RID
   * @return
   */
  def getResourceSimScore(simResources:scala.collection.Map[Int,scala.collection.immutable.Map[Int,Double]],
                          userRatingResource:Int, topSimResource:Int): Double ={
    simResources.get(topSimResource) match {
        case Some(sim) => sim.get(userRatingResource) match {
        case Some(score) => score
        case None => 0.0
      }
      case None => 0.0
    }
  }

  /**
   * lg 取10的对数
   * @param num 数字
   * @return
   */
  def log(num : Int) :Double ={
    math.log(num) / math.log(10)
  }

  /**
   * 保存流式处理推荐数据到Mysql
   * @param df 数据表
   * @param tableName 表名
   * @param mysqlConfig 配置
   */
  def saveStreamRecsToMysqlDB(df: DataFrame,tableName:String,mysqlConfig: MysqlConfig): Unit = {
    val mp = new Properties()
    mp.setProperty("user",mysqlConfig.user)
    mp.setProperty("password",mysqlConfig.password)
    df.write.mode(SaveMode.Overwrite).jdbc(mysqlConfig.url,tableName,mp)
  }

}
