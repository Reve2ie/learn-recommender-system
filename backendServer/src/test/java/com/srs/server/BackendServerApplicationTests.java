package com.srs.server;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.srs.server.entity.Recommendation;
import com.srs.server.entity.ResourceTags;
import com.srs.server.entity.vo.ResourceRecsVO;
import com.srs.server.entity.vo.TagVO;
import com.srs.server.mapper.ResourceRepoMapper;
import com.srs.server.service.ResourceRecsService;
import com.srs.server.service.ResourceRepoService;
import com.srs.server.service.ResourceTagsService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
class BackendServerApplicationTests {

//    @Autowired
//    MyRSAEncryptionUtils rsaUtils;
//
//    @Autowired
//    UserInfService userInfService;

//    @Autowired
//    private ElasticsearchRestTemplate template;

    @Autowired
    private ResourceRepoMapper mapper;

    @Autowired
    private ResourceRepoService repoService;

    @Autowired
    private ResourceRecsService recsService;

    @Autowired
    private ResourceTagsService tagsService;


    @Test
    void contextLoads() throws IOException {
//        rsaUtils.genKeyPair();
//        String message = "gzl799985";
//        System.out.println("随机生成的公钥为:" + rsaUtils.keyMap.get(0));
//        System.out.println("随机生成的私钥为:" + rsaUtils.keyMap.get(1));
//        String messageEn = rsaUtils.encrypt(message, rsaUtils.keyMap.get(0));
//        System.out.println("加密后的字符串为:" + messageEn);
//        String messageDe = rsaUtils.decrypt(messageEn, rsaUtils.keyMap.get(1));
//        System.out.println("还原后的字符串为:" + messageDe);
//        System.out.println(userInfService.getUserInfo(1));
//        System.out.println(template.toString());
    }

    @Test
    void test2(){
        System.out.println(recsService.getContentBasedTypeRecommendations("线性", 10));
    }

}
