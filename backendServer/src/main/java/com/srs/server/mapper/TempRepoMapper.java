package com.srs.server.mapper;

import com.srs.server.entity.TempRepo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Reverie
 * @since 2022-01-27
 */
public interface TempRepoMapper extends BaseMapper<TempRepo> {

}
