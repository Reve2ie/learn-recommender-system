package com.srs.server.mapper;

import com.srs.server.entity.Recommendation;
import com.srs.server.entity.ResourceRepo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.srs.server.entity.vo.ResourceInfoVO;
import com.srs.server.entity.vo.ResourceRecsVO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Reverie
 * @since 2022-01-27
 */
public interface ResourceRepoMapper extends BaseMapper<ResourceRepo> {

    // 返回推荐资源
    @Select({
            "<script>" +
                    "select r1.r_id,r1.resource_name,r1.provider,r1.size,r1.cover,r2.avg_score " +
                    "from resource_repo as r1,average_resources as r2 where r1.r_id=r2.r_id " +
                    "and r1.r_id in " +
                    "<foreach item = 'item' index = 'index' collection = 'ids' open='(' separator=',' close=')'>" +
                    "#{item}" +
                    "</foreach>"+
                    "</script>"})
    List<ResourceRecsVO> getResourceRecs(@Param("ids") List<Integer> ids);

    // 此方法在数据库中建立了视图
    // 返回资源详细信息
    @Select({
            "select * from resource_info where r_id = #{rid}"
    })
    ResourceInfoVO getResourceInfo(@Param("rid") Integer rid);

    // 获得 Resource CF
    @Select({
            "select r_id2 as rid,score from resource_recs where r_id1 = #{rid}"
    })
    List<Recommendation> getResourceCF(@Param("rid") Integer rid);

    // 获得 User CF
    @Select({
            "select r_id as rid,score from user_recs where user_id = #{uid}"
    })
    List<Recommendation> getUserCF(@Param("uid") Integer uid);

    // 获得最近热门的资源
    @Select({
            "select r_id as rid from rate_more_recently_resources order by date,count desc limit #{max}"
    })
    List<Integer> getHotRecommendations(@Param("max") Integer max);

    // 获得历史投票最多资源
    @Select({
            "select r_id as rid from rate_more_resources order by count desc limit #{max}"
    })
    List<Integer> getRateMoreRecommendations(@Param("max") Integer max);

    // 获得最近新添加资源
    // 返回推荐资源
    @Select({
            "select r1.r_id,r1.resource_name,r1.provider,r1.size,r1.cover,r2.avg_score " +
            "from resource_repo as r1,average_resources as r2 where r1.r_id=r2.r_id order by r1.add_time desc limit #{max}"})
    List<ResourceRecsVO> getNewResources(@Param("max") Integer max);

    // 获得用户自己评价资源ID
    @Select({
            "select r_id as rid from resource_ratings where user_id = #{uid}"})
    List<Integer> getMyRateResources(@Param("uid") Integer uid);

    // 获得对应类型的资源ID
    @Select({
            "<script>" +
                "select r_id as rid from type_resource where type_id in " +
                "<foreach item = 'item' index = 'index' collection = 'tids' open='(' separator=',' close=')'>" +
                "#{item}" +
                "</foreach> limit #{max}"+
                "</script>"})
    List<Integer> getTypeResources(@Param("tids") List<Integer> tids,@Param("max") Integer max);
}
