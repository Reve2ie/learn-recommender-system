package com.srs.server.mapper;

import com.srs.server.entity.UserInf;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.srs.server.entity.vo.admin.AdminInfoVO;
import com.srs.server.entity.vo.UserInfoVO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Reverie
 * @since 2022-01-27
 */
public interface UserInfMapper extends BaseMapper<UserInf> {

    @Select("SELECT user_login.user_id,user_login.login_name,user_inf.user_name,user_inf.major,user_inf.avatar,user_inf.gender,user_inf.email " +
            "FROM user_inf,user_login " +
            "WHERE user_inf.user_id = user_login.user_id AND user_login.user_id = #{userId}")
    UserInfoVO getUserInfo(@Param("userId") Integer userId);

    @Select("SELECT user_login.user_id,user_login.login_name,user_inf.user_name,user_inf.major,user_inf.avatar,user_inf.gender,user_inf.email,user_permission.user_role,user_permission.user_permission " +
            "FROM user_inf,user_login,user_permission " +
            "WHERE user_inf.user_id = user_login.user_id " +
            "AND user_inf.user_id = user_permission.user_id " +
            "AND user_login.user_id = #{userId}")
    AdminInfoVO getAdminInfo(@Param("userId") Integer userId);
}
