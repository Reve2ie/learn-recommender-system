package com.srs.server.common.lang;

import org.apache.http.HttpStatus;

/**
 * 状态码枚举
 */
public enum ResultCodeEnum {
    SUCCESS(true,0,"操作成功!"),
    UNKNOWN_ERROR(false,-1,"未知错误!"),
    PARAM_ERROR(false,-2,"参数错误!"),
    VALID(false,-3,"实体校验异常!"),
    UNAUTHORIZED(false,HttpStatus.SC_UNAUTHORIZED,"未经授权!");

    // 响应是否成功
    private Boolean success;

    // 响应状态码
    private Integer code;

    // 响应信息
    private String message;

    ResultCodeEnum(Boolean success, Integer code, String message) {
        this.success = success;
        this.code = code;
        this.message = message;
    }

    public Boolean getSuccess() {
        return success;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
