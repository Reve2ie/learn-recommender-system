package com.srs.server.service.impl;

import com.srs.server.entity.UserInf;
import com.srs.server.entity.vo.admin.AdminInfoVO;
import com.srs.server.entity.vo.UserInfoVO;
import com.srs.server.mapper.UserInfMapper;
import com.srs.server.service.UserInfService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Reverie
 * @since 2022-01-27
 */
@Service
public class UserInfServiceImpl extends ServiceImpl<UserInfMapper, UserInf> implements UserInfService {

    @Autowired
    UserInfMapper userInfMapper;

    @Override
    public UserInfoVO getUserInfo(Integer userId) {
        return userInfMapper.getUserInfo(userId);
    }

    @Override
    public AdminInfoVO getAdminInfo(Integer userId) {
        return userInfMapper.getAdminInfo(userId);
    }
}
