package com.srs.server.service;

import com.srs.server.entity.ResourceStatus;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Reverie
 * @since 2022-01-27
 */
public interface ResourceStatusService extends IService<ResourceStatus> {

}
