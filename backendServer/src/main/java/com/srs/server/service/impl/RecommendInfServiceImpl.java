package com.srs.server.service.impl;

import com.srs.server.entity.RecommendInf;
import com.srs.server.mapper.RecommendInfMapper;
import com.srs.server.service.RecommendInfService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Reverie
 * @since 2022-01-27
 */
@Service
public class RecommendInfServiceImpl extends ServiceImpl<RecommendInfMapper, RecommendInf> implements RecommendInfService {

}
