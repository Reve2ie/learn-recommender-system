package com.srs.server.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.srs.server.entity.Recommendation;
import com.srs.server.entity.ResourceType;
import com.srs.server.mapper.ResourceRepoMapper;
import com.srs.server.mapper.ResourceTypeMapper;
import com.srs.server.service.ResourceRecsService;
import com.srs.server.utils.RedisUtils;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.index.query.MultiMatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ResourceRecsServiceImpl implements ResourceRecsService {

    // 混合推荐中CF的比例
    private static Double CF_RATING_FACTOR = 0.4;
//    private static Double CB_RATING_FACTOR = 0.3;
    private static Double SR_RATING_FACTOR = 0.6;

    @Autowired
    ResourceRepoMapper repoMapper;

    @Autowired
    RedisUtils redisUtils;

    @Autowired
    ResourceTypeMapper typeMapper;

    @Autowired
    private TransportClient esClient;

    // 协同过滤推荐 Resource CF
    @Override
    public List<Recommendation> findResourceCFRecs(int rid, int maxItems) {
        List<Recommendation> cf = repoMapper.getResourceCF(rid);
        cf.sort(new Comparator<Recommendation>() {
            @Override
            public int compare(Recommendation o1, Recommendation o2) {
                return o1.getScore() > o2.getScore() ? -1 : 1;
            }
        });
        return cf.subList(0, Math.min(maxItems, cf.size()));
    }

    // 协同过滤推荐 User CF
    @Override
    public List<Recommendation> findUserCFRecs(int uid, int maxItems) {
        List<Recommendation> userCF = repoMapper.getUserCF(uid);
        userCF.sort(new Comparator<Recommendation>() {
            @Override
            public int compare(Recommendation o1, Recommendation o2) {
                return o1.getScore() > o2.getScore() ? -1 : 1;
            }
        });
        return userCF.subList(0, Math.min(maxItems, userCF.size()));
    }

    // 实时推荐
    @Override
    public List<Recommendation> findStreamRecs(int uid, int maxItems) {
        List<String> recsList = redisUtils.lRange("recs-uid:" + String.valueOf(uid), 0, -1);
        ArrayList<Recommendation> recommendations = new ArrayList<>();
        if (recsList == null || recsList.isEmpty()){
            return recommendations;
        }
        for (String recs : recsList) {
            String[] sp = recs.split(":");
            recommendations.add(new Recommendation(Integer.parseInt(sp[0]),Double.valueOf(sp[1])));
        }
        Collections.sort(recommendations, new Comparator<Recommendation>() {
            @Override
            public int compare(Recommendation o1, Recommendation o2) {
                return o1.getScore() > o2.getScore() ? -1 : 1;
            }
        });
        return recommendations.subList(0, Math.min(maxItems, recommendations.size()));
    }

    // 混合推荐 用户个性化
    @Override
    public List<Recommendation> getHybridRecommendations(int productId, int maxItems) {
        ArrayList<Recommendation> hybridRecommendations = new ArrayList<>();

        List<Recommendation> CFRecs = findUserCFRecs(productId, maxItems);
        for (Recommendation rec : CFRecs) {
            hybridRecommendations.add(new Recommendation(rec.getRid(),rec.getScore() * CF_RATING_FACTOR));
        }

        List<Recommendation> streamRecs = findStreamRecs(productId, maxItems);
        for (Recommendation rec : streamRecs) {
            hybridRecommendations.add(new Recommendation(rec.getRid(),rec.getScore() * SR_RATING_FACTOR));
        }
        hybridRecommendations.sort(new Comparator<Recommendation>() {
            @Override
            public int compare(Recommendation o1, Recommendation o2) {
                return o1.getScore() > o2.getScore() ? -1 : 1;
            }
        });
        return hybridRecommendations.subList(0, Math.min(maxItems, hybridRecommendations.size()));
    }

    // 离线推荐
    @Override
    public List<Recommendation> getCollaborativeFilteringRecommendations(int uid, int maxItems) {
        return findUserCFRecs(uid, maxItems);
    }

    // 相似推荐
    @Override
    public List<Recommendation> getCollaborativeFilteringRecommendations(int rid, int maxItems, int l) {
        return findResourceCFRecs(rid,maxItems);
    }

    // 获得热门资源
    @Override
    public List<Recommendation> getHotRecommendations(int maxItems) {
        List<Integer> ids = repoMapper.getHotRecommendations(maxItems);
        ArrayList<Recommendation> recommendations = new ArrayList<>();
        for (Integer id : ids) {
            recommendations.add(new Recommendation(id,0D));
        }
        return recommendations;
    }

    // 获得历史投票最多资源
    @Override
    public List<Recommendation> getRateMoreRecommendations(int maxItems) {
        List<Integer> ids = repoMapper.getRateMoreRecommendations(maxItems);
        ArrayList<Recommendation> recommendations = new ArrayList<>();
        for (Integer id : ids) {
            recommendations.add(new Recommendation(id,0D));
        }
        return recommendations;
    }

    // 全文检索
    @Override
    public List<Recommendation> getContentBasedSearchRecommendations(String text, int maxItems) {
        MultiMatchQueryBuilder query = QueryBuilders.multiMatchQuery(text, "resource_name", "description", "provider");
        return parseESResponse(esClient.prepareSearch().setIndices("recommender").setTypes("Resource")
                .setQuery(query).setSize(maxItems).execute().actionGet());
    }

    // 类别检索
    @Override
    public List<Recommendation> getContentBasedTypeRecommendations(String text, int maxItems) {
        List<ResourceType> type = typeMapper.selectList(new QueryWrapper<ResourceType>()
                .like("type_name",text));
        ArrayList<Integer> tids = new ArrayList<>();
        for (ResourceType t : type) {
            tids.add(t.getTypeId());
        }
        List<Integer> rids = repoMapper.getTypeResources(tids,maxItems);
        ArrayList<Recommendation> recommendations = new ArrayList<>();
        for (Integer rid : rids) {
            recommendations.add(new Recommendation(rid,0D));
        }
        return recommendations;
    }

    // ES查询
    @Override
    public List<Recommendation> parseESResponse(SearchResponse response) {
        ArrayList<Recommendation> recommendations = new ArrayList<>();
        for (SearchHit hit : response.getHits()) {
            recommendations.add(new Recommendation((int) hit.getSourceAsMap().get("r_id"), (double) hit.getScore()));
        }
        return recommendations;
    }
}
