package com.srs.server.service;

import com.srs.server.entity.UserInf;
import com.baomidou.mybatisplus.extension.service.IService;
import com.srs.server.entity.vo.admin.AdminInfoVO;
import com.srs.server.entity.vo.UserInfoVO;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Reverie
 * @since 2022-01-27
 */
public interface UserInfService extends IService<UserInf> {

    UserInfoVO getUserInfo(Integer userId);

    AdminInfoVO getAdminInfo(Integer userId);
}
