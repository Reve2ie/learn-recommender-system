package com.srs.server.service.impl;

import com.srs.server.entity.UserLogin;
import com.srs.server.mapper.UserLoginMapper;
import com.srs.server.service.UserLoginService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Reverie
 * @since 2022-01-27
 */
@Service
public class UserLoginServiceImpl extends ServiceImpl<UserLoginMapper, UserLogin> implements UserLoginService {

}
