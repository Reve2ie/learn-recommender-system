package com.srs.server.service;

import com.srs.server.entity.Recommendation;
import com.srs.server.entity.ResourceRepo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.srs.server.entity.vo.ResourceInfoVO;
import com.srs.server.entity.vo.ResourceRecsVO;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Reverie
 * @since 2022-01-27
 */
public interface ResourceRepoService extends IService<ResourceRepo> {

     // 推荐资源
     List<ResourceRecsVO> getRecommendResources(List<Recommendation> recommendations);

     // 依据ID 获取资源信息
     List<ResourceRecsVO> getResources(ArrayList<Integer> ids);

     // 资源详细信息
     ResourceInfoVO getResourceInfo(Integer rid);

     // 获得最近新添加的资源
     List<ResourceRecsVO> getNewResources(int maxItems);

     // 获取用户评分过的资源
     List<ResourceRecsVO> getMyRateResource(int uid);
}
