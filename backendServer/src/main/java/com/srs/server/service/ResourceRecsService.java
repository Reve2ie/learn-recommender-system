package com.srs.server.service;

import com.baomidou.mybatisplus.extension.api.R;
import com.srs.server.entity.Recommendation;
import org.elasticsearch.action.search.SearchResponse;

import java.util.List;

public interface ResourceRecsService {

    // 协同过滤推荐【资源相似度】
    List<Recommendation> findResourceCFRecs(int rid, int maxItems);

    // 协同过滤推荐【用户资源矩阵】
    List<Recommendation> findUserCFRecs(int uid, int maxItems);

    // 实时推荐
    List<Recommendation> findStreamRecs(int uid,int maxItems);

    // 混合推荐算法 协同+实时
    List<Recommendation> getHybridRecommendations(int productId, int maxItems);

    // 协调过滤推荐(离线)
    List<Recommendation> getCollaborativeFilteringRecommendations(int uid, int maxItems);

    // 协调过滤推荐(相似)
    List<Recommendation> getCollaborativeFilteringRecommendations(int rid, int maxItems, int l);

    // 热门资源推荐
    List<Recommendation> getHotRecommendations(int maxItems);

    // 历史投票最多推荐
    List<Recommendation> getRateMoreRecommendations(int maxItems);

    // 模糊查询资源
    List<Recommendation> getContentBasedSearchRecommendations(String text , int maxItems);

    // 查询类别资源
    List<Recommendation> getContentBasedTypeRecommendations(String text, int maxItems);

    // ES查询
    List<Recommendation> parseESResponse(SearchResponse response);
}
