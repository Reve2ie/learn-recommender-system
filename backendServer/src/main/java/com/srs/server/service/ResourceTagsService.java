package com.srs.server.service;

import com.srs.server.entity.ResourceTags;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Reverie
 * @since 2022-01-27
 */
public interface ResourceTagsService extends IService<ResourceTags> {

}
