package com.srs.server.service.impl;

import com.srs.server.entity.ResourceType;
import com.srs.server.mapper.ResourceTypeMapper;
import com.srs.server.service.ResourceTypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Reverie
 * @since 2022-01-27
 */
@Service
public class ResourceTypeServiceImpl extends ServiceImpl<ResourceTypeMapper, ResourceType> implements ResourceTypeService {

}
