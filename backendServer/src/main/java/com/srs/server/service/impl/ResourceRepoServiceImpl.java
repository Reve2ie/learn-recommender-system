package com.srs.server.service.impl;

import com.srs.server.entity.Recommendation;
import com.srs.server.entity.ResourceRepo;
import com.srs.server.entity.vo.ResourceInfoVO;
import com.srs.server.entity.vo.ResourceRecsVO;
import com.srs.server.mapper.ResourceRepoMapper;
import com.srs.server.service.ResourceRepoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Reverie
 * @since 2022-01-27
 */
@Service
public class ResourceRepoServiceImpl extends ServiceImpl<ResourceRepoMapper, ResourceRepo> implements ResourceRepoService {

    @Autowired
    ResourceRepoMapper mapper;

    @Override
    public List<ResourceRecsVO> getRecommendResources(List<Recommendation> recommendations) {
        ArrayList<Integer> ids = new ArrayList<>();
        for (Recommendation rec : recommendations) {
            ids.add(rec.getRid());
        }
        return getResources(ids);
    }

    @Override
    public List<ResourceRecsVO> getResources(ArrayList<Integer> ids) {
        List<ResourceRecsVO> resources = mapper.getResourceRecs(ids);
        resources.sort((o1, o2) -> o1.getAvgScore() > o2.getAvgScore() ? -1 : 1);
        return resources;
    }

    @Override
    public ResourceInfoVO getResourceInfo(Integer rid) {
        return mapper.getResourceInfo(rid);
    }

    @Override
    public List<ResourceRecsVO> getNewResources(int maxItems) {
        List<ResourceRecsVO> resources = mapper.getNewResources(maxItems);
        return resources;
    }

    @Override
    public List<ResourceRecsVO> getMyRateResource(int uid) {
        List<Integer> rids = mapper.getMyRateResources(uid);
        List<ResourceRecsVO> resources = getResources((ArrayList<Integer>) rids);
        return resources;
    }
}
