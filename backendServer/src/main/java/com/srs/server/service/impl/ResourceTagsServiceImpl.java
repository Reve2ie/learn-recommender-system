package com.srs.server.service.impl;

import com.srs.server.entity.ResourceTags;
import com.srs.server.mapper.ResourceTagsMapper;
import com.srs.server.service.ResourceTagsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Reverie
 * @since 2022-01-27
 */
@Service
public class ResourceTagsServiceImpl extends ServiceImpl<ResourceTagsMapper, ResourceTags> implements ResourceTagsService {

}
