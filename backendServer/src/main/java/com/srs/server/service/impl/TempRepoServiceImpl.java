package com.srs.server.service.impl;

import com.srs.server.entity.TempRepo;
import com.srs.server.mapper.TempRepoMapper;
import com.srs.server.service.TempRepoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Reverie
 * @since 2022-01-27
 */
@Service
public class TempRepoServiceImpl extends ServiceImpl<TempRepoMapper, TempRepo> implements TempRepoService {

}
