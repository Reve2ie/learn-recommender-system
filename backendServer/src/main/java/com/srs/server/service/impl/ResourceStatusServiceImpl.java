package com.srs.server.service.impl;

import com.srs.server.entity.ResourceStatus;
import com.srs.server.mapper.ResourceStatusMapper;
import com.srs.server.service.ResourceStatusService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Reverie
 * @since 2022-01-27
 */
@Service
public class ResourceStatusServiceImpl extends ServiceImpl<ResourceStatusMapper, ResourceStatus> implements ResourceStatusService {

}
