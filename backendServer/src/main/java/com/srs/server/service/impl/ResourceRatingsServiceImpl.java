package com.srs.server.service.impl;

import com.srs.server.entity.ResourceRatings;
import com.srs.server.mapper.ResourceRatingsMapper;
import com.srs.server.service.ResourceRatingsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Reverie
 * @since 2022-01-27
 */
@Service
public class ResourceRatingsServiceImpl extends ServiceImpl<ResourceRatingsMapper, ResourceRatings> implements ResourceRatingsService {

}
