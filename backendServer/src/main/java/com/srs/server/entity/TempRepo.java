package com.srs.server.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.time.LocalDate;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author Reverie
 * @since 2022-01-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class TempRepo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 自增ID
     */
    @TableId(value = "t_id", type = IdType.AUTO)
    private Integer tId;

    /**
     * 资源名称
     */
    private String resourceName;

    /**
     * 上传者
     */
    private String uploader;

    /**
     * 描述
     */
    private String description;

    /**
     * 唯一标识符
     */
    private String uuid;

    /**
     * 上传日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm")
    private LocalDateTime uploadTime;

    /**
     * 大小
     */
    private Long size;


}
