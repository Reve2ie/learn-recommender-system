package com.srs.server.entity.vo;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDateTime;

public class TagVO {

    /**
     * 资源ID
     */
    private Integer rId;

    /**
     * 标签
     */
    private String tag;


    @JsonFormat(pattern="yyyy-MM-dd HH:mm")
    private LocalDateTime time;

}
