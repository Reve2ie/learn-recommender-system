package com.srs.server.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.time.LocalDate;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author Reverie
 * @since 2022-01-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ResourceType implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 类别自增ID
     */
    @TableId(value = "type_id", type = IdType.AUTO)
    private Integer typeId;

    /**
     * 类别名称
     */
    private String typeName;

    /**
     * 资源总数
     */
    private Integer resourceTotal;

    /**
     * 添加日期
     */
    @JsonFormat(pattern="yyyy-MM-dd")
    private LocalDate addDate;

    /**
     * 最后修改日期
     */
    @JsonFormat(pattern="yyyy-MM-dd")
    private LocalDate modifiedDate;

    /**
     * 点赞总和
     */
    private Integer totalLike;

    /**
     * 下载总和
     */
    private Integer totalDownloads;


}
