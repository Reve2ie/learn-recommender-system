package com.srs.server.entity.vo;


import lombok.Data;

/**
 * 资源推荐VO
 */
@Data
public class ResourceRecsVO {

    private Integer rId;

    private String resourceName;

    private String provider;

    private Integer size;

    private Double avgScore;

    private String cover;
}
