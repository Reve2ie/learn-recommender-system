package com.srs.server.entity.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * 用户信息VO
 */
@Data
public class UserInfoVO implements Serializable {

    /**
     * 用户ID
     */
    private Integer userId;

    /**
     * 用户登录名
     */
    private String loginName;

    /**
     * 用户昵称
     */
    private String userName;

    /**
     * 专业方向
     */
    private String major;

    /**
     * 头像路径
     */
    private String avatar;

    /**
     * 性别 1男 0女
     */
    private Integer gender;

    /**
     * 邮箱
     */
    private String email;
}
