package com.srs.server.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.time.LocalDate;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author Reverie
 * @since 2022-01-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class RecommendInf implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 推荐ID
     */
    @TableId(value = "re_id", type = IdType.AUTO)
    private Integer reId;

    /**
     * 推荐内容
     */
    private String reName;

    /**
     * 适合专业
     */
    private String major;

    /**
     * 适合年级
     */
    private String grade;

    /**
     * 标签 每一项用“|”分割 
     */
    private String tags;

    /**
     * 添加时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm")
    private LocalDateTime addTime;


}
