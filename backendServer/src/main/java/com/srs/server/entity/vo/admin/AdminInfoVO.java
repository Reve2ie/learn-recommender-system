package com.srs.server.entity.vo.admin;

import lombok.Data;

@Data
public class AdminInfoVO {

    /**
     * 用户ID
     */
    private Integer userId;

    /**
     * 用户登录名
     */
    private String loginName;

    /**
     * 用户昵称
     */
    private String userName;

    /**
     * 专业方向
     */
    private String major;

    /**
     * 头像路径
     */
    private String avatar;

    /**
     * 性别 1男 0女
     */
    private Integer gender;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 角色
     */
    private String userRole;

    /**
     * 权限
     */
    private String userPermission;
}
