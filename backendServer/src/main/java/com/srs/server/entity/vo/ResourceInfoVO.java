package com.srs.server.entity.vo;

import lombok.Data;

@Data
public class ResourceInfoVO {

    /**
     * 自增ID
     */
    private Integer rId;

    /**
     * 资源名称
     */
    private String resourceName;

    /**
     * 资源格式
     */
    private String resourceFormat;

    /**
     * 资源类别ID
     */
    private Integer typeId;

    /**
     * 提供者
     */
    private String provider;

    /**
     * 描述
     */
    private String description;

    /**
     * 唯一标识符
     */
    private String uuid;

    /**
     * 大小
     */
    private Long size;

    /**
     * 图片URI
     */
    private String cover;

    /**
     * 点赞量
     */
    private Integer likes;

    /**
     * 下载量
     */
    private Integer downloads;

    /**
     * 状态 r-推荐 h-火热 d-默认
     */
    private String status;
}
