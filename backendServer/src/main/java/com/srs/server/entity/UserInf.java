package com.srs.server.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.Email;

/**
 * <p>
 * 
 * </p>
 *
 * @author Reverie
 * @since 2022-01-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("user_inf")
public class UserInf implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户信息自增ID
     */
    @TableId(value = "user_inf_id", type = IdType.AUTO)
    private Integer userInfId;

    /**
     * 用户登录表的用户登录ID
     */
    private Integer userId;

    /**
     * 用户昵称
     */
    private String userName;

    /**
     * 专业方向
     */
    private String major;

    /**
     * 头像路径
     */
    private String avatar;

    /**
     * 性别 1男 0女
     */
    private Integer gender;

    /**
     * 电话
     */
    private String phone;

    /**
     * 邮箱
     */
    @Email
    private String email;

    /**
     * 注册时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm")
    private LocalDateTime registerTime;

    /**
     * 最后修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm")
    private LocalDateTime modifiedTime;


}
