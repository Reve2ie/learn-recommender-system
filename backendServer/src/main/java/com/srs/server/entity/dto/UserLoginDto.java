package com.srs.server.entity.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;


@Data
public class UserLoginDto implements Serializable {

    /**
     * 用户登录名
     */
    @NotBlank(message = "用户名不能为空")
    private String loginName;

    /**
     * 用户登录密码
     */
    @NotBlank(message = "密码不能为空")
    private String password;

}
