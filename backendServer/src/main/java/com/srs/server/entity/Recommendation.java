package com.srs.server.entity;

/**
 * 推荐项目的包装
 */
public class Recommendation {

    // 资源ID
    private int rid;

    // 资源的推荐得分
    private Double score;

    public Recommendation() {
    }

    public Recommendation(int mid, Double score) {
        this.rid = mid;
        this.score = score;
    }

    public int getRid() {
        return rid;
    }

    public void setRid(int rid) {
        this.rid = rid;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    @Override
    public String toString() {
        return "Recommendation{" +
                "rid=" + rid +
                ", score=" + score +
                '}';
    }
}

