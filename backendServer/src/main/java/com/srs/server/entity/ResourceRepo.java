package com.srs.server.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.time.LocalDate;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author Reverie
 * @since 2022-01-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ResourceRepo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 自增ID
     */
    @TableId(value = "r_id", type = IdType.AUTO)
    private Integer rId;

    /**
     * 资源名称
     */
    private String resourceName;

    /**
     * 资源格式
     */
    private String resourceFormat;

    /**
     * 资源类别ID
     */
    private Integer typeId;

    /**
     * 提供者
     */
    private String provider;

    /**
     * 描述
     */
    private String description;

    /**
     * 唯一标识符
     */
    private String uuid;

    /**
     * 上传日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm")
    private LocalDateTime uploadTime;

    /**
     * 添加日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm")
    private LocalDateTime addTime;

    /**
     * 最后修改日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm")
    private LocalDateTime modifiedTime;

    /**
     * 大小
     */
    private Long size;

    /**
     * 图片URI
     */
    private String cover;

}
