package com.srs.server.entity.dto;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Data
public class UserRegisterDto {

    /**
     * 用户登录名
     */
    @NotBlank(message = "用户名不能为空！")
    private String loginName;

    /**
     * 登录密码
     */
    @NotBlank(message = "密码不能为空！")
    private String password;

    /**
     * 重复第二次密码
     */
    @NotBlank(message = "重复密码不能为空！")
    private String confirm;

    /**
     * 用户名
     */
    @NotBlank(message = "昵称不能为空！")
    private String userName;

    /**
     * 电话
     */
    @NotBlank(message = "电话不能为空！")
    private String phone;

    /**
     * 专业方向
     */
    private String major;

    /**
     * 性别 1男 0女
     */
    private Integer gender;

    /**
     * 邮箱
     */
    @Email
    private String email;
}
