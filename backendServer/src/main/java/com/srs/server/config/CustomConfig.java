package com.srs.server.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Configuration
@Component
@ConfigurationProperties(prefix = "srs.config")
public class CustomConfig {

    // 项目名称
    public static String name;

    // 版本
    public static String version;

    // 年份
    private static String copyrightYear;

    // 基本路径
    private static String basePath;

    public static String getName() {
        return name;
    }

    public void setName(String name) {
        CustomConfig.name = name;
    }

    public static String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        CustomConfig.version = version;
    }

    public static String getCopyrightYear() {
        return copyrightYear;
    }

    public void setCopyrightYear(String copyrightYear) {
        CustomConfig.copyrightYear = copyrightYear;
    }

    public static String getBasePath() {
        return basePath;
    }

    public void setBasePath(String basePath) {
        CustomConfig.basePath = basePath;
    }

    // 获得临时资源前缀uri
    public static String getTempPath(){
        return getBasePath() + "/temp/";
    }
    // 获得资源仓库前缀uri
    public static String getRepoPath(){
        return getBasePath() + "/repo/";
    }
    // 获得头像前缀uri
    public static String getAvatarPath(){
        return getBasePath() + "/avatar/";
    }
    // 获得图像前缀uri
    public static String getPicPath(){
        return getBasePath() + "/pic/";
    }
}
