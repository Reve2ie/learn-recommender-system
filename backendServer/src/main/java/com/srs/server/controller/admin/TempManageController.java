package com.srs.server.controller.admin;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.srs.server.common.lang.Result;
import com.srs.server.common.lang.ResultCodeEnum;
import com.srs.server.config.CustomConfig;
import com.srs.server.entity.ResourceRepo;
import com.srs.server.entity.ResourceStatus;
import com.srs.server.entity.ResourceType;
import com.srs.server.entity.TempRepo;
import com.srs.server.service.ResourceRepoService;
import com.srs.server.service.ResourceStatusService;
import com.srs.server.service.ResourceTypeService;
import com.srs.server.service.TempRepoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;

@RestController
@RequestMapping("/manage/temp")
@Slf4j
public class TempManageController {

    @Autowired
    TempRepoService tempRepoService;

    @Autowired
    ResourceTypeService typeService;

    @Autowired
    ResourceRepoService repoService;

    @Autowired
    ResourceStatusService statusService;

    /**
     * 获得临时资源信息
     * @param currentPage 当前页数
     * @param pageSize 页面个数
     * @return
     */
    @GetMapping("/get")
    public Result getTempResources(Integer currentPage, Integer pageSize){
        if(currentPage == null || currentPage < 1) currentPage = 1;
        if(pageSize == null || pageSize < 1) pageSize = 10;
        Page page = new Page(currentPage,pageSize);
        IPage pageData = tempRepoService.page(page, new QueryWrapper<TempRepo>().orderByDesc("upload_time"));
        return Result.setResult(ResultCodeEnum.SUCCESS).data("data",pageData);
    }

    /**
     * 搜索临时资源信息
     * @param currentPage 当前页数
     * @param pageSize 页面个数
     * @param resourceName 资源关键字
     * @return
     */
    @GetMapping("/search")
    public Result searchTempResources(Integer currentPage, Integer pageSize,String resourceName){
        if(currentPage == null || currentPage < 1) currentPage = 1;
        if(pageSize == null || pageSize < 1) pageSize = 10;
        Page page = new Page(currentPage,pageSize);
        IPage pageData = tempRepoService.page(page, new QueryWrapper<TempRepo>()
                .orderByDesc("upload_time")
                .like("resource_name",resourceName));
        return Result.setResult(ResultCodeEnum.SUCCESS).data("data",pageData);
    }

    /**
     * 移动至资源仓库
     * @param tempId  临时资源ID
     * @param typeName 资源类型名
     * @return
     * @throws IOException
     */
    @PostMapping("/move")
    public Result removeToRepo(Integer tempId,String typeName) throws IOException {
        Assert.notNull(tempId,"参数不能为空！");
        Assert.notNull(typeName,"参数不能为空！");

        TempRepo temp = tempRepoService.getById(tempId);
        Assert.notNull(temp,"无此资源信息！");

        ResourceType type = typeService.getOne(new QueryWrapper<ResourceType>().eq("type_name", typeName));
        Assert.notNull(type,"无此资源类型！");

        // 文件移动
        String resourceName = temp.getResourceName();
        String tempFilePath = CustomConfig.getTempPath() + resourceName;
        File tempFile = new File(tempFilePath);
        if (!tempFile.exists()){
            return Result.setResult(ResultCodeEnum.VALID).message("资源不存在！");
        }

        ResourceRepo er = repoService.getOne(new QueryWrapper<ResourceRepo>().eq("resource_name", temp.getResourceName()));
        Assert.isNull(er,"已存在此资源！");

        // 文件不为空，删除原有文件并在仓库创建新文件
        String resourcePath = CustomConfig.getRepoPath() + typeName + "/" + resourceName;
        File repoResource = new File(resourcePath);
        // 检测是否存在该目录
        if (!repoResource.getParentFile().exists()){
            repoResource.setWritable(true, false);
            repoResource.getParentFile().mkdirs();
        }
        if (!repoResource.exists()){
            repoResource.createNewFile();
        }

        // 输入流 输出流
        try (FileInputStream in = new FileInputStream(tempFile); FileOutputStream out = new FileOutputStream(repoResource, true)) {
            byte[] buffer = new byte[1024];
            while (true) {
                int read = in.read(buffer);
                if (read == -1) break;
                out.write(buffer, 0, read);
            }
        } catch (IOException e) {
            log.error("转移异常:-------------->{}", resourceName + e.getMessage());
            return Result.setResult(ResultCodeEnum.VALID).message(resourceName + e.getMessage());
        }

        // 资源信息封装
        ResourceRepo resourceRepo = new ResourceRepo();
        resourceRepo.setResourceName(resourceName);
        resourceRepo.setDescription(temp.getDescription());
        resourceRepo.setResourceFormat(resourceName.substring(resourceName.lastIndexOf(".")+1));
        resourceRepo.setSize(temp.getSize());
        resourceRepo.setProvider(temp.getUploader());
        resourceRepo.setUuid(temp.getUuid());
        resourceRepo.setTypeId(type.getTypeId());
        resourceRepo.setAddTime(LocalDateTime.now());
        resourceRepo.setModifiedTime(LocalDateTime.now());
        resourceRepo.setUploadTime(LocalDateTime.now());
        repoService.save(resourceRepo);

        ResourceRepo getId = repoService.getOne(new QueryWrapper<ResourceRepo>().eq("resource_name", resourceName));
        ResourceStatus rs = new ResourceStatus();
        rs.setRId(getId.getRId());
        rs.setStatus("d");
        rs.setDownloads(0);
        rs.setLikes(0);
        statusService.save(rs);

        // 资源类型信息封装
        type.setResourceTotal(type.getResourceTotal() + 1);
        type.setModifiedDate(LocalDate.now());
        typeService.updateById(type);

        if (!tempFile.delete()){
           return Result.setResult(ResultCodeEnum.VALID).message("删除临时资源失败！");
        }
        tempRepoService.removeById(tempId);
        return Result.ok().message("移动成功！");
    }


    /**
     * 删除临时资源
     * @param tid ID
     * @return
     */
    @PostMapping("/del/{tid}")
    public Result delTempResource(@PathVariable String tid){
        TempRepo temp = tempRepoService.getById(tid);
        Assert.notNull(temp,"无此资源信息！");

        String tempPath = CustomConfig.getTempPath() + temp.getResourceName();
        File tempFile = new File(tempPath);
        if (!tempFile.exists()){
            return Result.setResult(ResultCodeEnum.VALID).message("无此资源！");
        }

        tempFile.delete();
        tempRepoService.removeById(tid);
        return Result.setResult(ResultCodeEnum.SUCCESS).message("删除成功！");
    }

}
