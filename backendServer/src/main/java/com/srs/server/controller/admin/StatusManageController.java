package com.srs.server.controller.admin;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.srs.server.common.lang.Result;
import com.srs.server.entity.ResourceRepo;
import com.srs.server.entity.ResourceStatus;
import com.srs.server.service.ResourceStatusService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


/**
 * 资源状态控制器
 */
@Slf4j
@RestController
@RequestMapping("/manage/status")
public class StatusManageController {

    @Autowired
    private ResourceStatusService statusService;

    @GetMapping("/get")
    public Result getResourceStatus(Integer currentPage, Integer pageSize){
        if(currentPage == null || currentPage < 1) currentPage = 1;
        if(pageSize == null || pageSize < 1) pageSize = 10;
        Page page = new Page(currentPage,pageSize);
        IPage pageData = statusService.page(page, new QueryWrapper<ResourceStatus>().orderByDesc("s_id"));
        return Result.ok().data("data",pageData);
    }

    @GetMapping("/search")
    public Result searchResourceStatus(Integer currentPage, Integer pageSize, @NotBlank String rid){
        if(currentPage == null || currentPage < 1) currentPage = 1;
        if(pageSize == null || pageSize < 1) pageSize = 10;
        Page page = new Page(currentPage,pageSize);
        IPage pageData = statusService.page(page, new QueryWrapper<ResourceStatus>()
                .orderByDesc("s_id")
                .eq("r_id",rid));
        return Result.ok().data("data",pageData);
    }

    @PostMapping("/update")
    public Result updateResourceStatus(@NotNull Integer rid, @NotNull Integer like, @NotNull Integer downloads, @NotBlank String status){
        ResourceStatus rs = statusService.getOne(new QueryWrapper<ResourceStatus>()
                .eq("r_id", rid));
        Assert.notNull(rs,"不存在此状态消息！");

        rs.setDownloads(downloads);
        rs.setLikes(like);
        rs.setStatus(status);
        statusService.updateById(rs);
        return Result.ok();
    }
}
