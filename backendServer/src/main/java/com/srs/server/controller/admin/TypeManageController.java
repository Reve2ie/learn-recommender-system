package com.srs.server.controller.admin;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.srs.server.common.lang.Result;
import com.srs.server.common.lang.ResultCodeEnum;
import com.srs.server.config.CustomConfig;
import com.srs.server.entity.ResourceRepo;
import com.srs.server.entity.ResourceType;
import com.srs.server.service.ResourceRepoService;
import com.srs.server.service.ResourceTypeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.time.LocalDate;


@RestController
@RequestMapping("/manage/type")
@Slf4j
public class TypeManageController {

    @Autowired
    private ResourceTypeService typeService;

    @Autowired
    private ResourceRepoService repoService;

    /**
     * 获得资源类型信息
     * @param currentPage 页数
     * @param pageSize 个数
     * @return
     */
    @GetMapping("/get")
    public Result getResourceTypes(Integer currentPage, Integer pageSize){
        if(currentPage == null || currentPage < 1) currentPage = 1;
        if(pageSize == null || pageSize < 1) pageSize = 10;
        Page page = new Page(currentPage,pageSize);
        IPage pageData = typeService.page(page, new QueryWrapper<ResourceType>()
                .orderByDesc("add_date"));
        return Result.ok().data("data",pageData);
    }

    /**
     * 搜索资源类型信息
     * @param currentPage 页数
     * @param pageSize 个数
     * @param typeName 类型关键字
     * @return
     */
    @GetMapping("/search")
    public Result searchResourceTypes(Integer currentPage, Integer pageSize,String typeName){
        if(currentPage == null || currentPage < 1) currentPage = 1;
        if(pageSize == null || pageSize < 1) pageSize = 10;
        Page page = new Page(currentPage,pageSize);
        IPage pageData = typeService.page(page, new QueryWrapper<ResourceType>()
                .orderByDesc("add_date")
                .like("type_name",typeName));
        return Result.ok().data("data",pageData);
    }

    /**
     * 添加新类型
     * @param typeName 类型名
     * @return
     */
    @PostMapping("/add")
    public Result addType(String typeName){
        ResourceType et = typeService.getOne(new QueryWrapper<ResourceType>().eq("type_name", typeName));
        Assert.isNull(et,"已经存在此类型!");

        // 新类型封装
        ResourceType type = new ResourceType();
        type.setTypeName(typeName);
        type.setResourceTotal(0);
        type.setModifiedDate(LocalDate.now());
        type.setTotalDownloads(0);
        type.setTotalLike(0);
        type.setAddDate(LocalDate.now());

        typeService.save(type);
        return Result.ok();
    }

    /**
     * 更新类型信息
     * @param typeId 类型ID
     * @param newTypeName 新类型名
     * @param newCount 资源数量
     * @return
     */
    @PostMapping("/update")
    public Result updateType(String typeId,String newTypeName,Integer newCount){
        ResourceType et = typeService.getById(typeId);
        Assert.notNull(et,"不存在此类型!");

        // 资源类型更改
        if (!et.getTypeName().equals(newTypeName)){
            File oldFile = new File(CustomConfig.getRepoPath() + et.getTypeName());
            File newFile = new File(CustomConfig.getRepoPath() + newTypeName);
            oldFile.renameTo(newFile);
        }

        et.setTypeName(newTypeName);
        et.setResourceTotal(newCount);
        et.setModifiedDate(LocalDate.now());

        typeService.updateById(et);
        return Result.ok();
    }

    /**
     * 删除类型 需要不存在此类型资源才能删除
     * @param tid 类型ID
     * @return
     */
    @PostMapping("/delete/{tid}")
    public Result delType(@PathVariable String tid){
        ResourceType et = typeService.getById(tid);
        Assert.notNull(et,"不存在此类型!");

        int count = repoService.count(new QueryWrapper<ResourceRepo>()
                .eq("type_id", et.getTypeId()));
        if (count != 0){
            return Result.setResult(ResultCodeEnum.PARAM_ERROR).message("该类型还存在资源文件！");
        }

        File file = new File(CustomConfig.getRepoPath() + et.getTypeName());
        file.delete();
        typeService.removeById(tid);
        return Result.ok();
    }
}
