package com.srs.server.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.srs.server.common.lang.Result;
import com.srs.server.common.lang.ResultCodeEnum;
import com.srs.server.config.CustomConfig;
import com.srs.server.entity.*;
import com.srs.server.entity.vo.ResourceInfoVO;
import com.srs.server.service.*;
import com.srs.server.utils.MyUtils;
import com.srs.server.utils.RedisUtils;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;

/**
 * 前端控制器
 */
@Slf4j
@RestController
@RequestMapping("/op")
public class ResourceRestController {

    @Autowired
    UserLoginService userLoginService;

    @Autowired
    ResourceRepoService repoService;

    @Autowired
    ResourceStatusService resourceStatusService;

    @Autowired
    TempRepoService tempRepoService;

    @Autowired
    ResourceTypeService typeService;

    @Autowired
    ResourceRatingsService ratingsService;

    @Autowired
    ResourceTagsService tagsService;

    @Autowired
    RedisUtils redisUtils;

    private final static String BASE_TEMP_PATH = CustomConfig.getTempPath();
    private final static String BASE_REPO_PATH = CustomConfig.getRepoPath();

    private static Logger logger = LoggerFactory.getLogger(ResourceRestController.class.getName());

    /**
     * 下载
     * @param uuid 资源唯一标识符
     * @param response 回应
     */
    @GetMapping("/download/{uuid}")
    public Result download(@PathVariable String uuid,  HttpServletResponse response){
        ResourceRepo er = repoService.getOne(new QueryWrapper<ResourceRepo>().eq("uuid", uuid));
        Assert.notNull(er,"资源不存在！");

        ResourceType et = typeService.getById(er.getTypeId());
        Assert.notNull(et,"类型不存在！");

        String rp = BASE_REPO_PATH + et.getTypeName() + "/" + er.getResourceName();
        File resourceFile = new File(rp);
        if (!resourceFile.exists()){
            Result.setResult(ResultCodeEnum.VALID).message("资源文件不存在！");
        }

        // 资源流式传输
        try {
            response.setContentType("application/force-download");// 设置强制下载不打开
            response.addHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(er.getResourceName(),"UTF-8"));
            response.setHeader("Content-Length",String.valueOf(resourceFile.length()));

            byte[] buffer = new byte[1024];
            try (FileInputStream fis = new FileInputStream(resourceFile); BufferedInputStream bis = new BufferedInputStream(fis)) {
                OutputStream os = response.getOutputStream();
                int i = bis.read(buffer);
                while (i != -1) {
                    os.write(buffer, 0, i);
                    i = bis.read(buffer);
                }
            } catch (Exception e) {
                log.error("传输异常:-------------->{}", er.getResourceName() + e.getMessage());
                e.printStackTrace();
            }
        }catch (Exception e){
            log.error("下载异常:-------------->{}",er.getResourceName()+e.getMessage());
            e.printStackTrace();
        }

        // 更新数据
        ResourceStatus resourceStatus = resourceStatusService.getOne(new QueryWrapper<ResourceStatus>()
                .eq("r_id", er.getRId()));
        resourceStatus.setDownloads(resourceStatus.getDownloads() + 1);
        resourceStatusService.updateById(resourceStatus);
        et.setTotalDownloads(et.getTotalDownloads() + 1);
        typeService.updateById(et);

        return null;
    }

    /**
     * 上传资源
     * @param files 资源
     * @param uploader 上传者
     * @param description 描述
     * @return
     */
    @PostMapping("/upload")
    public Result upload(@RequestParam(value = "files") MultipartFile[] files, String uploader, String description){
        Assert.notNull(files,"资源文件不能为空！");
        for (MultipartFile file : files) {
            String fileName = file.getOriginalFilename();
            TempRepo erf = tempRepoService.getOne(new QueryWrapper<TempRepo>().eq("resource_name", fileName));
            Assert.isNull(erf,fileName+"-已经存在同名文件！");

            // 设置文件存储路径
            String resourceTempPath = BASE_TEMP_PATH + fileName;
            File temp = new File(resourceTempPath);
            // 判断父目录是否存在
            if (!temp.getParentFile().exists()){
                temp.setWritable(true,false);
                temp.getParentFile().mkdirs();
            }
            try {
                file.transferTo(temp);
            } catch (IOException e) {
                log.error("上传异常:-------------->{}",fileName+ " " +e.getMessage());
                return Result.setResult(ResultCodeEnum.UNKNOWN_ERROR).message(fileName+"文件上传异常，请稍后再试！");
            }

            // Info To DB
            TempRepo tempRepo = new TempRepo();
            tempRepo.setResourceName(fileName);
            tempRepo.setDescription(description);
            tempRepo.setUploader(uploader);
            tempRepo.setUploadTime(LocalDateTime.now());
            tempRepo.setUuid(MyUtils.getUuid());
            tempRepo.setSize(file.getSize() / 1024 ); // KB
            tempRepoService.save(tempRepo);
        }
        return Result.ok().message("上传成功！");
    }

    /**
     * 资源评分
     * @param rid 资源ID
     * @param uid 用户ID
     * @param score 评分
     * @param comment 评论
     * @return
     */
    @GetMapping(value = "/rate/{rid}" , produces = "application/json")
    public Result rateToResource(@PathVariable("rid")int rid,@RequestParam("uid")Integer uid, @RequestParam("score")Double score,@RequestParam("comment")String comment){
        UserLogin user = userLoginService.getById(uid);
        Assert.notNull(user,"用户不存在！");

        ResourceRatings er = ratingsService.getOne(new QueryWrapper<ResourceRatings>()
                .eq("user_id", uid)
                .eq("r_id", rid));

        // redis 添加数据
        if (redisUtils.hasKey("uid:"+uid) && redisUtils.lLen("uid:"+uid) >= 40){
            redisUtils.lRightPop("uid:"+uid);
        }
        redisUtils.lLeftPush("uid:"+uid, rid+":"+score);

        boolean complete = false;
        if (er != null){
            er.setScore(score);
            er.setComment(comment);
            er.setTime(LocalDateTime.now());
            complete = ratingsService.updateById(er);
        }else {
            ResourceRatings ratings = new ResourceRatings();
            ratings.setRId(rid);
            ratings.setUserId(uid);
            ratings.setScore(score);
            ratings.setComment(comment);
            ratings.setTime(LocalDateTime.now());
            complete = ratingsService.save(ratings);
        }

        // 埋点日志
        if(complete) {
            System.out.print("=========complete=========");
            logger.info("MOVIE_RATING_PREFIX" + ":" + uid +"|"+ rid +"|"+ score +"|"+ System.currentTimeMillis()/1000);
        }
        return Result.ok().message("评分成功！");
    }

    /**
     * 添加标签
     * @param rid 资源ID
     * @param uid 用户ID
     * @param tag 标签
     * @return
     */
    @GetMapping("/tag/{rid}")
    public Result addTags(@PathVariable("rid")int rid,@RequestParam("uid")Integer uid ,@RequestParam("tag") String tag){
        UserLogin user = userLoginService.getById(uid);
        Assert.notNull(user,"用户不存在！");

        // 封装标签信息
        ResourceTags newTag = new ResourceTags();
        newTag.setTag(tag);
        newTag.setUserId(uid);
        newTag.setRId(rid);
        newTag.setTime(LocalDateTime.now());
        tagsService.save(newTag);

        // 更新ES 信息

        return Result.ok().message("标签添加成功！");
    }

    /**
     * 点赞+1
     * @param rid 资源ID
     * @return
     */
    @GetMapping("/like/{rid}")
    public Result incrLike(@PathVariable String rid){
        ResourceStatus es = resourceStatusService.getOne(new QueryWrapper<ResourceStatus>().eq("r_id", rid));
        Assert.notNull(es,"不存在此资源状态信息");

        es.setLikes(es.getLikes()+1);
        resourceStatusService.updateById(es);
        return Result.ok().message("点赞成功！");
    }

}
