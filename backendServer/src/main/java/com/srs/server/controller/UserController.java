package com.srs.server.controller;


import cn.hutool.crypto.SecureUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.srs.server.common.lang.Result;
import com.srs.server.common.lang.ResultCodeEnum;
import com.srs.server.entity.UserInf;
import com.srs.server.entity.UserLogin;
import com.srs.server.entity.UserPermission;
import com.srs.server.entity.dto.UserLoginDto;
import com.srs.server.entity.dto.UserRegisterDto;
import com.srs.server.entity.vo.UserInfoVO;
import com.srs.server.service.UserInfService;
import com.srs.server.service.UserLoginService;
import com.srs.server.service.UserPermissionService;
import com.srs.server.utils.JwtUtils;
import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.time.LocalDateTime;

/**
 * <p>
 *  前端控制器
 *  用户req-res
 * </p>
 *
 * @author Reverie
 * @since 2022-01-27
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserLoginService userLoginService;

    @Autowired
    UserInfService userInfService;

    @Autowired
    UserPermissionService userPermissionService;

    @Autowired
    JwtUtils jwtUtils;

    /**
     * 用户登录
     * @param uld 登录对象
     * @param response 回应
     * @return
     */
    @PostMapping("/login")
    public Result userLogin(@Valid @RequestBody UserLoginDto uld, HttpServletResponse response){
        UserLogin loginUser = userLoginService.getOne(new QueryWrapper<UserLogin>().eq("login_name", uld.getLoginName()));
        Assert.notNull(loginUser,"用户不存在！");
        if (!loginUser.getPassword().equals(SecureUtil.md5(uld.getPassword()))) {
            return Result.setResult(ResultCodeEnum.PARAM_ERROR).message("密码错误！");
        }

        // Header返回jwt
        String jwt = jwtUtils.generateToken(loginUser.getUserId());
        response.setHeader("token",jwt);
        response.setHeader("Access-Control-Expose-Headers", "token");

        // 更新登录时间
        userLoginService.update(new UpdateWrapper<UserLogin>()
                .eq("user_id",loginUser.getUserId())
                .set("login_time", LocalDateTime.now()));

        // Body返回用户信息
        UserInfoVO userInfo = userInfService.getUserInfo(loginUser.getUserId());
        return Result.setResult(ResultCodeEnum.SUCCESS).data("user_info",userInfo).message("登录成功！");
    }

    @PostMapping("/register")
    public Result register(@Valid @RequestBody UserRegisterDto urd){
        UserLogin user = userLoginService.getOne(new QueryWrapper<UserLogin>().eq("login_name", urd.getLoginName()));
        Assert.isNull(user,"用户已存在！");
        if (!urd.getPassword().equals(urd.getConfirm())){
            return Result.setResult(ResultCodeEnum.PARAM_ERROR).message("两次密码输入不同！");
        }

        // 用户信息封装
        UserLogin userLogin = new UserLogin();
        userLogin.setLoginName(urd.getLoginName());
        userLogin.setPassword(SecureUtil.md5(urd.getPassword()));
        userLogin.setStatus(0);
        userLogin.setModifiedTime(LocalDateTime.now());
        userLogin.setLoginTime(LocalDateTime.now());
        userLoginService.save(userLogin);

        UserLogin u = userLoginService.getOne(new QueryWrapper<UserLogin>().eq("login_name", urd.getLoginName()));
        UserInf userInf = new UserInf();
        userInf.setUserId(u.getUserId());
        userInf.setUserName(urd.getUserName());
        userInf.setEmail(urd.getEmail());
        userInf.setGender(urd.getGender());
        userInf.setPhone(urd.getPhone());
        userInf.setMajor(urd.getMajor());
        userInf.setRegisterTime(LocalDateTime.now());
        userInf.setModifiedTime(LocalDateTime.now());
        userInfService.save(userInf);

        UserPermission userPermission = new UserPermission();
        userPermission.setUserId(u.getUserId());
        userPermission.setUserRole("user");
        userPermission.setUserPermission("none");
        userPermissionService.save(userPermission);

        return Result.setResult(ResultCodeEnum.SUCCESS).message("注册成功！");
    }

    /* 注销 */
    @GetMapping("/logout")
    public Result logout(){
        SecurityUtils.getSubject().logout();
        return Result.setResult(ResultCodeEnum.SUCCESS).message("注销成功！");
    }
}
