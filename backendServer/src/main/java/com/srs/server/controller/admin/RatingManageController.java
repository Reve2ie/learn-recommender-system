package com.srs.server.controller.admin;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.srs.server.common.lang.Result;
import com.srs.server.entity.ResourceRatings;
import com.srs.server.entity.ResourceRepo;
import com.srs.server.service.ResourceRatingsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;

@RestController
@RequestMapping("/manage/rating")
@Slf4j
public class RatingManageController {

    @Autowired
    ResourceRatingsService ratingsService;

    @GetMapping("/get")
    public Result getResourceRatings(Integer currentPage, Integer pageSize){
        if(currentPage == null || currentPage < 1) currentPage = 1;
        if(pageSize == null || pageSize < 1) pageSize = 10;
        Page page = new Page(currentPage,pageSize);
        IPage pageData = ratingsService.page(page, new QueryWrapper<ResourceRatings>()
                .orderByDesc("time"));
        return Result.ok().data("data",pageData);
    }

    @GetMapping("/search/{op}/{id}")
    public Result searchResourceRatings(Integer currentPage, Integer pageSize, @NotBlank @PathVariable String op, @NotBlank @PathVariable String id){
        if(currentPage == null || currentPage < 1) currentPage = 1;
        if(pageSize == null || pageSize < 1) pageSize = 10;
        Page page = new Page(currentPage,pageSize);
        IPage pageData = ratingsService.page(page, new QueryWrapper<ResourceRatings>()
                .orderByDesc("time")
                .eq(op.equals("uid")?"user_id":"r_id",id));
        return Result.ok().data("data",pageData);
    }

    @PostMapping("/add")
    public Result addRatingInfo(@NotNull Integer uid, @NotNull Integer rid, @NotNull Double score, @NotBlank String comment){
        ResourceRatings rri = ratingsService.getOne(new QueryWrapper<ResourceRatings>()
                .eq("user_id", uid)
                .eq("r_id", rid));
        Assert.isNull(rri,"已存在评分消息！");

        // 封装添加评分消息
        ResourceRatings rr = new ResourceRatings();
        rr.setUserId(uid);
        rr.setRId(rid);
        rr.setScore(score);
        rr.setComment(comment);
        rr.setTime(LocalDateTime.now());
        ratingsService.save(rr);
        return Result.ok();
    }

    @PostMapping("/update")
    public Result updateRatingInfo(@NotNull Integer uid, @NotNull Integer rid, @NotNull Double newScore, @NotBlank String newComment){
        ResourceRatings rri = ratingsService.getOne(new QueryWrapper<ResourceRatings>()
                .eq("user_id", uid)
                .eq("r_id", rid));
        Assert.notNull(rri,"不存在评分消息！");

        // 封装更新评分消息
        rri.setScore(newScore);
        rri.setComment(newComment);
        rri.setTime(LocalDateTime.now());
        ratingsService.updateById(rri);
        return Result.ok();
    }

    @PostMapping("/delete")
    public Result delRatingInfo(Integer uid, Integer rid){
        boolean b = ratingsService.remove(new QueryWrapper<ResourceRatings>()
                .eq("user_id", uid)
                .eq("r_id", rid));
        if (!b){
            return Result.error().message("删除错误！");
        }
        return Result.ok();
    }
}
