package com.srs.server.controller;


import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.srs.server.common.lang.Result;
import com.srs.server.entity.Recommendation;
import com.srs.server.entity.ResourceTags;
import com.srs.server.entity.ResourceType;
import com.srs.server.entity.vo.ResourceInfoVO;
import com.srs.server.entity.vo.ResourceRecsVO;
import com.srs.server.entity.vo.TagVO;
import com.srs.server.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * <p>
 *  前端控制器
 *  用户推荐
 * </p>
 *
 * @author Reverie
 * @since 2022-01-27
 */
@RestController
@RequestMapping("/resource")
public class ResourceRecsController {

    @Autowired
    ResourceRepoService repoService;

    @Autowired
    ResourceRecsService recsService;

    @Autowired
    ResourceTypeService typeService;

    @Autowired
    ResourceTagsService tagsService;

    /**
     * 资源详细信息
     * @param rid
     * @return
     */
    @GetMapping("/info/{rid}")
    public Result getResourceInfo(@PathVariable Integer rid){
        ResourceInfoVO reInfo = repoService.getResourceInfo(rid);
        return Result.ok().data("info",reInfo);
    }

    /**
     * 获取所有类型
     * @return
     */
    @GetMapping("/types")
    public Result getTypes(){
        List<ResourceType> list = typeService.list();
        HashMap<Integer, String> map = new HashMap<>();
        for (ResourceType type : list) {
            map.put(type.getTypeId(),type.getTypeName());
        }
        return Result.ok().data("types",map);
    }

    /**
     * 资源对应标签
     * @param rid
     * @return
     */
    @GetMapping(value = "/tag/{rid}")
    public Result getResourceTags(@PathVariable Integer rid){
        List<ResourceTags> tags = tagsService.list(new QueryWrapper<ResourceTags>()
                .eq("r_id", rid));
        ArrayList<TagVO> tagvo = new ArrayList<>();
        for (ResourceTags tag : tags) {
            TagVO vo = new TagVO();
            BeanUtil.copyProperties(tag,vo);
            tagvo.add(vo);
        }
        return Result.ok().data("tags",tagvo);
    }

    /**
     * 资源内对应用户打上的标签
     * @param rid
     * @param uid
     * @return
     */
    @GetMapping(value = "/mytag/{rid}")
    public Result getResourceMyTags(@PathVariable Integer rid,@RequestParam Integer uid){
        List<ResourceTags> tags = tagsService.list(new QueryWrapper<ResourceTags>()
                .eq("r_id", rid)
                .eq("user_id",uid));
        ArrayList<TagVO> tagvo = new ArrayList<>();
        for (ResourceTags tag : tags) {
            TagVO vo = new TagVO();
            BeanUtil.copyProperties(tag,vo);
            tagvo.add(vo);
        }
        return Result.ok().data("mytags",tagvo);
    }


    /**
     * 个性化推荐
     * @param uid
     * @param num
     * @return
     */
    @GetMapping(value = "/personal", produces = "application/json")
    public Result getPersonalRecs(@RequestParam("uid")Integer uid, @RequestParam("num") int num){
        List<Recommendation> recommendations = recsService.getHybridRecommendations(uid, num);
        if (recommendations.size() == 0){
            // 无记录要返回默认
            return Result.ok().message("无数据信息！");
        }
        List<ResourceRecsVO> recs = repoService.getRecommendResources(recommendations);
        return Result.ok().data("recs",recs);
    }

    /**
     * 离线推荐
     * @param uid
     * @param num
     * @return
     */
    @GetMapping(value = "/offline" , produces = "application/json")
    public Result getOfflineRecs(@RequestParam("uid")Integer uid, @RequestParam("num") int num){
        List<Recommendation> recommendations = recsService.getCollaborativeFilteringRecommendations(uid,num);
        if (recommendations.size() == 0){
            // 无记录要返回默认
            return Result.ok().message("无数据信息！");
        }
        List<ResourceRecsVO> recs = repoService.getRecommendResources(recommendations);
        return Result.ok().data("recs",recs);
    }

    /**
     * 最近热门推荐
     * @param num
     * @return
     */
    @GetMapping(value = "/hot" , produces = "application/json")
    public Result getHotRecs(@RequestParam("num") int num){
        List<Recommendation> recommendations = recsService.getHotRecommendations(num);
        List<ResourceRecsVO> recs = repoService.getRecommendResources(recommendations);
        return Result.ok().data("recs",recs);
    }

    /**
     * 投票最多的资源
     * @param num
     * @return
     */
    @GetMapping(value = "/rate" , produces = "application/json")
    public Result getRateMoreResource(@RequestParam("num") int num){
        List<Recommendation> recommendations = recsService.getRateMoreRecommendations(num);
        List<ResourceRecsVO> recs = repoService.getRecommendResources(recommendations);
        return Result.ok().data("recs",recs);
    }

    /**
     * 获取最近新添加资源
     * @param num
     * @return
     */
    @GetMapping(value = "/new")
    public Result getNewResources(@RequestParam("num")int num){
        List<ResourceRecsVO> recs = repoService.getNewResources(num);
        return Result.ok().data("recs",recs);
    }

    /**
     * 获取资源详细页面相似的资源集合
     * @param rid
     * @param num
     * @return
     */
    @GetMapping(value = "/same/{rid}" , produces = "application/json")
    public Result getSameResources(@PathVariable Integer rid, @RequestParam("num")int num){
        List<Recommendation> recommendations = recsService.getCollaborativeFilteringRecommendations(rid, num,1);
        List<ResourceRecsVO> recs = repoService.getRecommendResources(recommendations);
        return Result.ok().data("recs",recs);
    }

    /**
     * 模糊查询资源
     * @param query
     * @return
     */
    @GetMapping(value = "/search" , produces = "application/json")
    public Result getSearchResources(@RequestParam("query")String query, @RequestParam("num")int num){
        List<Recommendation> recommendations = recsService.getContentBasedSearchRecommendations(query, num);
        List<ResourceRecsVO> search = repoService.getRecommendResources(recommendations);
        return Result.ok().data("search_info",search);
    }

    /**
     * 查询类别资源
     * @param query
     * @param num
     * @return
     */
    @GetMapping(value = "/type")
    public Result getTypeResources(@RequestParam("query")String query, @RequestParam("num")int num){
        List<Recommendation> recommendations = recsService.getContentBasedTypeRecommendations(query, num);
        List<ResourceRecsVO> search = repoService.getRecommendResources(recommendations);
        return Result.ok().data("search_info",search);
    }

    /**
     * 用户自己评分过的资源
     * @param uid
     * @return
     */
    @GetMapping(value = "/myrate", produces = "application/json")
    public Result getMyRateResources(@RequestParam("uid")Integer uid){
        List<ResourceRecsVO> recs = repoService.getMyRateResource(uid);
        return Result.ok().data("recs",recs);
    }
}
