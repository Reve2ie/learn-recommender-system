package com.srs.server.controller.admin;

import cn.hutool.crypto.SecureUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.srs.server.common.lang.Result;
import com.srs.server.common.lang.ResultCodeEnum;
import com.srs.server.entity.UserInf;
import com.srs.server.entity.UserLogin;
import com.srs.server.entity.UserPermission;
import com.srs.server.entity.dto.UserLoginDto;
import com.srs.server.entity.vo.admin.AdminInfoVO;
import com.srs.server.service.UserInfService;
import com.srs.server.service.UserLoginService;
import com.srs.server.service.UserPermissionService;
import com.srs.server.utils.JwtUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;


/**
 * 后台管理控制器
 */
@Slf4j
@RestController
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    UserLoginService userLoginService;

    @Autowired
    UserInfService userInfService;

    @Autowired
    UserPermissionService permissionService;

    @Autowired
    JwtUtils jwtUtils;

    /* ------------------ 获取 ------------------- */
    @GetMapping("/get/login-info")
    public Result getUserLoginInfo(Integer currentPage, Integer pageSize){
        if(currentPage == null || currentPage < 1) currentPage = 1;
        if(pageSize == null || pageSize < 1) pageSize = 10;
        Page page = new Page(currentPage,pageSize);
        IPage pageData = userLoginService.page(page, new QueryWrapper<UserLogin>()
                .orderByDesc("user_id"));
        return Result.ok().data("data",pageData);
    }

    @GetMapping("/get/user-info")
    public Result getUserInfo(Integer currentPage, Integer pageSize){
        if(currentPage == null || currentPage < 1) currentPage = 1;
        if(pageSize == null || pageSize < 1) pageSize = 10;
        Page page = new Page(currentPage,pageSize);
        IPage pageData = userInfService.page(page, new QueryWrapper<UserInf>()
                .orderByDesc("user_id"));
        return Result.ok().data("data",pageData);
    }

    @GetMapping("/get/permission-info")
    public Result getUserPermissionInfo(Integer currentPage, Integer pageSize){
        if(currentPage == null || currentPage < 1) currentPage = 1;
        if(pageSize == null || pageSize < 1) pageSize = 10;
        Page page = new Page(currentPage,pageSize);
        IPage pageData = permissionService.page(page, new QueryWrapper<UserPermission>()
                .orderByDesc("user_id"));
        return Result.ok().data("data",pageData);
    }

    /* ------------------ 搜索 ------------------- */
    @GetMapping("/search/login-info")
    public Result searchUserLoginInfo(Integer currentPage, Integer pageSize, String loginName){
        if(currentPage == null || currentPage < 1) currentPage = 1;
        if(pageSize == null || pageSize < 1) pageSize = 10;
        Page page = new Page(currentPage,pageSize);
        IPage pageData = userLoginService.page(page, new QueryWrapper<UserLogin>()
                .orderByDesc("user_id")
                .like("login_name",loginName));
        return Result.ok().data("data",pageData);
    }

    @GetMapping("/search/user-info")
    public Result searchUserInfo(Integer currentPage, Integer pageSize, String username){
        if(currentPage == null || currentPage < 1) currentPage = 1;
        if(pageSize == null || pageSize < 1) pageSize = 10;
        Page page = new Page(currentPage,pageSize);
        IPage pageData = userInfService.page(page, new QueryWrapper<UserInf>()
                .orderByDesc("user_id")
                .like("user_name", username));
        return Result.ok().data("data",pageData);
    }

    @GetMapping("/search/permission-info")
    public Result searchUserPermissionInfo(Integer currentPage, Integer pageSize, String uid){
        if(currentPage == null || currentPage < 1) currentPage = 1;
        if(pageSize == null || pageSize < 1) pageSize = 10;
        Page page = new Page(currentPage,pageSize);
        IPage pageData = permissionService.page(page, new QueryWrapper<UserPermission>()
                .orderByDesc("user_id")
                .eq("user_id",uid));
        return Result.ok().data("data",pageData);
    }


    /**
     * 管理员登录-后台
     * @param uld ID
     * @param response 回应
     * @return
     */
    @PostMapping("/login")
    public Result userLogin(@Valid @RequestBody UserLoginDto uld, HttpServletResponse response){
        UserLogin loginAdmin = userLoginService.getOne(new QueryWrapper<UserLogin>().eq("login_name", uld.getLoginName()));
        Assert.notNull(loginAdmin,"管理员不存在！");
        if (!loginAdmin.getPassword().equals(SecureUtil.md5(uld.getPassword()))) {
            return Result.setResult(ResultCodeEnum.PARAM_ERROR).message("密码错误！");
        }

        // Header返回jwt
        String jwt = jwtUtils.generateToken(loginAdmin.getUserId());
        response.setHeader("token",jwt);
        response.setHeader("Access-Control-Expose-Headers", "token");

        // 更新登录时间
        userLoginService.update(new UpdateWrapper<UserLogin>()
                .eq("user_id",loginAdmin.getUserId())
                .set("login_time", LocalDateTime.now()));

        // Body返回管理员信息
        AdminInfoVO info = userInfService.getAdminInfo(loginAdmin.getUserId());
        return Result.setResult(ResultCodeEnum.SUCCESS).data("admin_info",info).message("登录成功！");
    }

    /**
     * 添加管理员-后台添加的都是管理员
     * @param lname 用户登录名
     * @param password 密码
     * @param confirm 二次确认密码
     * @return
     */
    @PostMapping("/add")
    public Result addAdministrator(@NotBlank String lname, @NotBlank String password, @NotBlank String confirm){
        UserLogin ad = userLoginService.getOne(new QueryWrapper<UserLogin>()
                .eq("login_name", lname));
        Assert.isNull(ad,"已存在此用户！");

        if (!password.equals(confirm)){
            return Result.setResult(ResultCodeEnum.PARAM_ERROR).message("两次密码不一致！");
        }

        // 封装管理员信息
        UserLogin ul = new UserLogin();
        ul.setLoginName(lname);
        ul.setPassword(SecureUtil.md5(password));
        ul.setStatus(1);
        ul.setLoginTime(LocalDateTime.now());
        ul.setModifiedTime(LocalDateTime.now());
        userLoginService.save(ul);

        ad = userLoginService.getOne(new QueryWrapper<UserLogin>()
                .eq("login_name", lname));
        UserInf ui = new UserInf();
        ui.setUserId(ad.getUserId());
        ui.setRegisterTime(LocalDateTime.now());
        ui.setModifiedTime(LocalDateTime.now());
        ui.setUserName("default");
        userInfService.save(ui);

        UserPermission up = new UserPermission();
        up.setUserRole("admin");
        up.setUserPermission("curd");
        up.setUserId(ad.getUserId());
        permissionService.save(up);
        return Result.ok();
    }

    /**
     * 更新用户信息
     * @param uid 用户ID
     * @param newUserName 昵称名
     * @param newMajor 专业
     * @param newGender 性别 1男 0女
     * @param newPhone 手机号
     * @param newEmail 邮箱
     * @return
     */
    @PostMapping("/update/info")
    public Result updateUserInfo(@NotNull Integer uid, @NotBlank String newUserName, @NotBlank String newMajor,
                                  @NotNull Integer newGender, @NotBlank String newPhone, @NotBlank String newEmail){
        UserInf ui = userInfService.getOne(new QueryWrapper<UserInf>()
                .eq("user_id", uid));
        Assert.notNull(ui,"无此用户信息！");

        ui.setUserName(newUserName);
        ui.setMajor(newMajor);
        ui.setGender(newGender);
        ui.setPhone(newPhone);
        ui.setEmail(newEmail);
        ui.setModifiedTime(LocalDateTime.now());
        userInfService.updateById(ui);
        return Result.ok();
    }

    /**
     * 更新用户权限信息
     * @param uid ID
     * @param role 角色
     * @param permission 权限
     * @return
     */
    @PostMapping("/update/permission")
    public Result updateUserPermission(@NotNull Integer uid, @NotBlank String role, @NotBlank String permission){
        UserPermission up = permissionService.getOne(new QueryWrapper<UserPermission>()
                .eq("user_id", uid));
        Assert.notNull(up,"无此管理权限信息！");

        up.setUserRole(role);
        up.setUserPermission(permission);
        permissionService.updateById(up);
        return Result.ok();
    }

    /**
     * 修改密码
     * @param uid ID
     * @param oldPassword 旧密码
     * @param newPassword 新密码
     * @param confirm 二次确认密码
     * @return
     */
    @PostMapping("/pwd")
    public Result changePassword(@NotNull Integer uid, @NotBlank String oldPassword, @NotBlank String newPassword, @NotBlank String confirm){
        UserLogin ul = userLoginService.getById(uid);
        Assert.notNull(ul,"不存在此用户信息！");

        if (!SecureUtil.md5(oldPassword).equals(ul.getPassword())){
            return Result.setResult(ResultCodeEnum.PARAM_ERROR).message("用户密码错误！");
        }
        if (!newPassword.equals(confirm)){
            return Result.setResult(ResultCodeEnum.VALID).message("两次密码不一致！");
        }

        ul.setPassword(SecureUtil.md5(newPassword));
        ul.setModifiedTime(LocalDateTime.now());
        userLoginService.updateById(ul);
        return Result.ok();
    }

    @PostMapping("/delete/{uid}")
    public Result delUser(@PathVariable @NotNull Integer uid){
        UserLogin eul = userLoginService.getById(uid);
        Assert.notNull(eul,"不存在此用户！");

        UserPermission up = permissionService.getOne(new QueryWrapper<UserPermission>()
                .eq("user_id", uid));
        if (up.getUserRole().equals("super")){
            return Result.setResult(ResultCodeEnum.UNAUTHORIZED).message("无法删除！");
        }

        // 删除登录信息-身份信息-权限信息
        userLoginService.removeById(uid);
        userInfService.remove(new QueryWrapper<UserInf>().eq("user_id",uid));
        permissionService.remove(new QueryWrapper<UserPermission>().eq("user_id",uid));

        return Result.ok();
    }

    /* 注销 */
    @GetMapping("/logout")
    public Result logout(){
        SecurityUtils.getSubject().logout();
        return Result.setResult(ResultCodeEnum.SUCCESS).message("注销成功！");
    }

}
