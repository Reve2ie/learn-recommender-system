package com.srs.server.controller.admin;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.srs.server.common.lang.Result;
import com.srs.server.config.CustomConfig;
import com.srs.server.entity.ResourceRepo;
import com.srs.server.entity.ResourceStatus;
import com.srs.server.entity.ResourceType;
import com.srs.server.service.ResourceRepoService;
import com.srs.server.service.ResourceStatusService;
import com.srs.server.service.ResourceTypeService;
import com.srs.server.utils.RedisUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.time.LocalDate;
import java.time.LocalDateTime;

@RestController
@RequestMapping("/manage/repo")
@Slf4j
public class RepoManageController {

    @Autowired
    private ResourceRepoService repoService;

    @Autowired
    private ResourceTypeService typeService;

    @Autowired
    private ResourceStatusService statusService;

    @Autowired
    private RedisUtils redisUtils;

    /**
     * 获得资源信息
     * @param currentPage 页数
     * @param pageSize 个数
     * @return
     */
    @GetMapping("/get")
    public Result getRepoResources(Integer currentPage, Integer pageSize){
        if(currentPage == null || currentPage < 1) currentPage = 1;
        if(pageSize == null || pageSize < 1) pageSize = 10;
        Page page = new Page(currentPage,pageSize);
        IPage pageData = repoService.page(page, new QueryWrapper<ResourceRepo>().orderByDesc("add_time"));
        return Result.ok().data("data",pageData);
    }

    /**
     * 搜索资源信息
     * @param currentPage 页数
     * @param pageSize 个数
     * @param resourceName 资源名关键字
     * @return
     */
    @GetMapping("/search")
    public Result searchRepoResources(Integer currentPage, Integer pageSize, String resourceName){
        if(currentPage == null || currentPage < 1) currentPage = 1;
        if(pageSize == null || pageSize < 1) pageSize = 10;
        Page page = new Page(currentPage,pageSize);
        IPage pageData = repoService.page(page, new QueryWrapper<ResourceRepo>()
                .like("resource_name",resourceName)
                .orderByDesc("add_time"));
        return Result.ok().data("data",pageData);
    }

    /**
     * 更新资源信息
     * @param rId 资源ID
     * @param newProvider 新提供者
     * @param newResourceName 新资源名
     * @return
     */
    @PostMapping("/update")
    public Result updateResourceInfo(Integer rId,String newProvider,String newResourceName){
        ResourceRepo ur = repoService.getById(rId);
        Assert.notNull(ur,"无此资源信息！");

        if (!ur.getProvider().equals(newProvider)){
            ur.setProvider(newProvider);
        }

        ResourceType type = typeService.getById(ur.getTypeId());
        if (!ur.getResourceName().equals(newResourceName)){
            String oldPath = CustomConfig.getRepoPath() + type.getTypeName() + "/" + ur.getResourceName();
            String newPath = CustomConfig.getRepoPath() + type.getTypeName() + "/" + newResourceName;
            File file = new File(oldPath);
            if(!file.renameTo(new File(newPath))){
                return Result.error().message("文件修改失败！");
            }
            ur.setResourceName(newResourceName);
        }

        ur.setUploadTime(LocalDateTime.now());
        repoService.updateById(ur);
        return Result.ok();
    }

    /**
     * 删除资源
     * @param rid 资源ID
     * @return
     */
    @PostMapping("/delete/{rid}")
    public Result delRepoResource(@PathVariable String rid){
        ResourceRepo ur = repoService.getById(rid);
        Assert.notNull(ur,"无此资源信息！");
        ResourceType type = typeService.getById(ur.getTypeId());

        String path = CustomConfig.getRepoPath() + type.getTypeName() + "/" + ur.getResourceName();
        File file = new File(path);
        if (file.exists()){
            file.delete();
        }

        type.setResourceTotal(type.getResourceTotal()-1);
        type.setModifiedDate(LocalDate.now());
        typeService.updateById(type);

        statusService.remove(new QueryWrapper<ResourceStatus>().eq("r_id",ur.getRId()));
        repoService.removeById(ur.getRId());
        return Result.ok();
    }
}
