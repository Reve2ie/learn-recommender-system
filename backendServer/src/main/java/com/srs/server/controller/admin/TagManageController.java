package com.srs.server.controller.admin;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.srs.server.common.lang.Result;
import com.srs.server.entity.ResourceRatings;
import com.srs.server.entity.ResourceRepo;
import com.srs.server.entity.ResourceTags;
import com.srs.server.service.ResourceTagsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Slf4j
@RestController
@RequestMapping("/manage/tag")
public class TagManageController {

    @Autowired
    ResourceTagsService tagsService;

    @GetMapping("/get")
    public Result getResourceTags(Integer currentPage, Integer pageSize){
        if(currentPage == null || currentPage < 1) currentPage = 1;
        if(pageSize == null || pageSize < 1) pageSize = 10;
        Page page = new Page(currentPage,pageSize);
        IPage pageData = tagsService.page(page, new QueryWrapper<ResourceTags>()
                .orderByDesc("time"));
        return Result.ok().data("data",pageData);
    }

    @GetMapping("/search/{op}/{id}")
    public Result searchResourceTags(Integer currentPage, Integer pageSize, @PathVariable String id, @PathVariable String op){
        if(currentPage == null || currentPage < 1) currentPage = 1;
        if(pageSize == null || pageSize < 1) pageSize = 10;
        Page page = new Page(currentPage,pageSize);
        IPage pageData = tagsService.page(page, new QueryWrapper<ResourceTags>()
                .orderByDesc("time")
                .like(op.equals("uid")?"user_id":"r_id",id));
        return Result.ok().data("data",pageData);
    }

    @PostMapping("/add")
    public Result addTagInfo(@NotNull Integer uid, @NotNull Integer rid , @NotBlank String tag){
        ResourceTags et = tagsService.getOne(new QueryWrapper<ResourceTags>()
                .eq("user_id", uid)
                .eq("r_id", rid)
                .eq("tag", tag));
        Assert.isNull(et,"你已经打过此标签！");

        // 封装标签信息
        ResourceTags newTag = new ResourceTags();
        newTag.setTag(tag);
        newTag.setUserId(uid);
        newTag.setRId(rid);
        newTag.setTime(LocalDateTime.now());
        tagsService.save(newTag);
        return Result.ok();
    }

    @PostMapping("/delete")
    public Result delTagInfo(@NotNull Integer uid, @NotNull Integer rid , @NotBlank String tag){
        boolean b = tagsService.remove(new QueryWrapper<ResourceTags>()
                .eq("user_id", uid)
                .eq("r_id", rid)
                .eq("tag", tag));
        if (!b){
            return Result.error().message("删除错误！");
        }
        return Result.ok();
    }
}
