package com.srs.server.shiro;

import com.srs.server.entity.UserLogin;
import com.srs.server.entity.UserPermission;
import com.srs.server.service.UserLoginService;
import com.srs.server.service.UserPermissionService;
import com.srs.server.utils.JwtUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class AccountRealm extends AuthorizingRealm {

    @Autowired
    JwtUtils jwtUtils;

    @Autowired
    UserLoginService userLoginService;

    @Autowired
    UserPermissionService userPermissionService;

    // 让realm支持jwt的凭证校验
    @Override
    public boolean supports(AuthenticationToken token) {
        return token instanceof JwtToken;
    }

    // 认证
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        JwtToken jwt = (JwtToken) authenticationToken;
        log.info("jwt----------------->{}", jwt);
        String id = jwtUtils.getClaimByToken((String) jwt.getPrincipal()).getSubject();
        UserLogin ul = userLoginService.getById(id);
        if (ul == null){
            throw new UnknownAccountException("账号不存在！");
        }
        if (ul.getStatus() == -1){
            throw new LockedAccountException("账户已被锁定！");
        }
        UserPermission up = userPermissionService.getById(ul.getUserId());
        AccountProfile profile = new AccountProfile();
        profile.setUserId(ul.getUserId());
        profile.setLoginName(ul.getLoginName());
        profile.setStatus(ul.getStatus());
        profile.setRole(up.getUserRole());
        profile.setPermission(up.getUserPermission());
        log.info("profile----------------->{}", profile.toString());
        return new SimpleAuthenticationInfo(profile,jwt.getCredentials(),getName());
    }

    // 授权
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        AccountProfile principal = (AccountProfile) principalCollection.getPrimaryPrincipal();
        SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();
        authorizationInfo.addStringPermission(principal.getPermission());
        return authorizationInfo;
    }
}