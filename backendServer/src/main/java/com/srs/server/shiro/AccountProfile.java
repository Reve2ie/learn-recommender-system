package com.srs.server.shiro;

import lombok.Data;

import java.io.Serializable;

@Data
public class AccountProfile implements Serializable {
    private Integer userId;
    private String loginName;
    private Integer status;
    private String role;
    private String permission;
}
